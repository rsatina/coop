<?php

namespace App\Listeners;

use App\Events\AppointmentCreated;
use App\TimeSlot;

class DecrementTimeSlotCount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\AppointmentCreated  $event
     * @return void
     */
    public function handle(AppointmentCreated $event)
    {
        $timeSlotId = $event->appointment->time_slot_id;

        TimeSlot::where('id', $timeSlotId)->decrement('slot_count');
    }

}