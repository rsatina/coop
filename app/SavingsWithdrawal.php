<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavingsWithdrawal extends Model
{
    public function borrower()
    {
        return $this->belongsTo('Borrower');
    }
}
