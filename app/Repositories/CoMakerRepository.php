<?php

namespace App\Repositories;

use App\CoMaker;
use Carbon\Carbon;
use App\Repositories\CoMakerRepositoryInterface;

class CoMakerRepository implements CoMakerRepositoryInterface
{
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getById($id)
    {
        return CoMaker::find($id);
    }

    public function getByBorrowerId($borrower_id)
    {
        return CoMaker::where('borrower_id', $borrower_id)->get();
    }

    public function getDetailsByBorrowerAndComakerId($id, $borrower_id)
    {
        return CoMaker::join('borrowers', 'co_makers.borrower_id', '=', 'borrowers.id')
            ->join('loans', 'co_makers.borrower_id', '=', 'loans.borrower_id')
            ->select('loans.*', 'borrowers.*',
                     'co_makers.firstname as cm_firstname',
                     'co_makers.lastname as cm_lastname',
                     'co_makers.middlename as cm_middlename',
                     'co_makers.comaker_email',
                     'co_makers.contact_no as cm_contact_no',
                     'co_makers.employer_name as cm_employer_name',
                     'co_makers.position as cm_position')
            ->where('co_makers.id', $id)
            ->get();
    }

    public function add($comaker)
    {
        $model = new CoMaker;

        $model->borrower_id = $comaker['borrower_id'];
        $model->lastname = $comaker['lastname'];
        $model->firstname = $comaker['firstname'];
        $model->middlename = $comaker['middlename'];
        $model->comaker_email = $comaker['comaker_email'];
        $model->contact_no = $comaker['contact_no'];
        $model->employer_name = $comaker['employer_name'];
        $model->position = $comaker['position'];
        $model->employment_status = $comaker['employment_status'];
        $model->yrs_employment = $comaker['yrs_employment'];

        $model->save();

        return $model;
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }

    public function getComakerByEmail($email)
    {
        return CoMaker::join('loans', 'co_makers.borrower_id', '=', 'loans.borrower_id')
            ->join('borrowers', 'co_makers.borrower_id', '=', 'borrowers.id')
            ->select('borrowers.*', 'loans.*')
            ->where('comaker_email', $email)
            ->get();

    }
}