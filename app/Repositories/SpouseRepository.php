<?php

namespace App\Repositories;

use App\Spouse;
use Carbon\Carbon;

class SpouseRepository implements SpouseRepositoryInterface
{
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getById($id)
    {
        return Spouse::find($id);
    }

    public function getByBorrowerId($borrower_id)
    {
        return Spouse::where('borrower_id', $borrower_id)->get();
    }
    public function add($spouse)
    {
        $model = new Spouse;

        $model->borrower_id = $spouse['borrower_id'];
        $model->lastname = $spouse['spouse_last_name'];
        $model->firstname = $spouse['spouse_first_name'];
        $model->middlename = $spouse['spouse_middle_name'];
        $model->spouse_email = $spouse['spouse_email'];
        $model->contact_no = $spouse['spouse_contact_no'];
        $model->employer_name = $spouse['spouse_employer_name'];
        $model->position = $spouse['spouse_position'];
        $model->employment_status = $spouse['spouse_employment_status'];
        $model->yrs_employment = $spouse['spouse_years_of_employment'];

        $model->save();

        return $model;
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }
}