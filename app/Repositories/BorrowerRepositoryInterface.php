<?php

namespace App\Repositories;

interface BorrowerRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function add($borrower);

    public function update($id);

    public function remove($id);
}