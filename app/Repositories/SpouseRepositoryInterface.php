<?php

namespace App\Repositories;

interface SpouseRepositoryInterface
{
    public function getAll();

    public function getById($id);
    public function getByBorrowerId($borrower_id);

    public function add($spouse);

    public function update($id);

    public function remove($id);
}