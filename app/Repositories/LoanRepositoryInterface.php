<?php

namespace App\Repositories;

interface LoanRepositoryInterface
{
    public function getAll();
    public function getAllByUserId($user_id);
    public function getAllTransactions();
    public function getByBorrowerId($borrower_id);

    public function getById($id);

    public function add($loan);

    public function update($id);

    public function remove($id);
}