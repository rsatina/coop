<?php

namespace App\Repositories;

interface SavingsWithdrawalRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function add($withdrawal);

    public function update($id);

    public function remove($id);
}