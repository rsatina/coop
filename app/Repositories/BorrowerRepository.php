<?php

namespace App\Repositories;

use App\Borrower;
use Carbon\Carbon;

class BorrowerRepository implements BorrowerRepositoryInterface
{
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getById($id)
    {
        return Borrower::find($id);
    }

    public function add($borrower)
    {
        $model = new Borrower;

        $model->user_id = $borrower['user_id'];
        $model->lipiemco_staff = $borrower['lipiemco'];
        $model->lastname = $borrower['borrower_last_name'];
        $model->firstname = $borrower['borrower_first_name'];
        $model->middlename = $borrower['borrower_middle_name'];
        $model->present_address = $borrower['borrower_present_address'];
        $model->no_yrs_stay = $borrower['borrower_years_of_stay'];
        $model->place_of_residence = $borrower['mode_of_stay'];
        $model->property_owner = $borrower['property_owner'];
        $model->provincial_address = $borrower['provincial_address'];
        $model->borrower_email = $borrower['borrower_email'];
        $model->contact_no = $borrower['borrower_contact_no'];
        $model->employer_name = $borrower['borrower_employer_name'];
        $model->position = $borrower['borrower_position'];
        $model->employer_status = $borrower['employment_status'];
        $model->yrs_employment = $borrower['borrower_years_of_employment'];

        $model->save();

        return $model;
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }
}