<?php

namespace App\Repositories;

use App\Loan;
use App\SavingsWithdrawal;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LoanRepository implements LoanRepositoryInterface
{
    public function getAll()
    {
        return Loan::join('borrowers', 'loans.borrower_id', '=', 'borrowers.id')
            ->select('loans.*', 'borrowers.firstname', 'borrowers.lastname')
            ->orderBy('id','desc')
            ->get();
    }

    public function getAllByUserId($user_id)
    {
        return Loan::join('borrowers', 'loans.borrower_id', '=', 'borrowers.id')
            ->select('loans.*', 'borrowers.user_id')
            ->where('borrowers.user_id', $user_id)
            ->orderBy('loans.id', 'desc')
            ->get();
    }

    public function getByBorrowerId($borrower_id)
    {
        return Loan::join('borrowers', 'loans.borrower_id', '=', 'borrowers.id')
            ->select('loans.*', 'borrowers.*')
            ->where('loans.borrower_id', $borrower_id)
            ->get();
    }

    public function getAllTransactions()
    {
        return Loan::with('savings_withdrawals')
        ->get();
    }

    public function getById($id)
    {
        return Loan::find($id);
    }

    public function add($loan)
    {
        $model = new Loan;

        $model->borrower_id = $loan['borrower_id'];
        $model->loan_application_no = $loan['loan_application_no'];
        $model->loan_type = $loan['loan_type'];
        $model->amount_word = $loan['amount_applied'];
        $model->amount_figure = $loan['amount'];
        $model->term_applied = $loan['term_applied'];
        $model->mode_of_payment = $loan['mode_payment'];
        $model->status = $loan['status'];
        $model->purpose = $loan['purpose'];
        $model->interest_rate = $loan['interest_rate'];
        $model->special_savings = $loan['special_savings'];
        $model->service_charge = $loan['service_charge'];

        $model->save();

        return $model;
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }

    public function updateStatus($id, $status)
    {
        $model = Loan::find($id);
        $model->status = $status;

        $model->save();
        return $model;
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }

}