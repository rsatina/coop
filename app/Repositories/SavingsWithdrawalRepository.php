<?php

namespace App\Repositories;

use App\SavingsWithdrawal;

class SavingsWithdrawalRepository implements SavingsWithdrawalRepositoryInterface
{
    public function getAll()
    {
        return SavingsWithdrawal::orderBy('id', 'desc')->get();
    }

    public function getAllByUserId($user_id)
    {
        return SavingsWithdrawal::where('user_id', $user_id)
            ->orderBy('id', 'desc')
            ->get();
    }
    public function getById($id)
    {
        return SavingsWithdrawal::find($id);
    }

    public function add($withdrawal)
    {
        $model = new SavingsWithdrawal();

        $model->user_id = $withdrawal['user_id'];
        $model->savings_withdrawal_no = $withdrawal['savings_withdrawal_no'];
        $model->savings_type = $withdrawal['savings_type'];
        $model->firstname = $withdrawal['firstname'];
        $model->lastname = $withdrawal['lastname'];
        $model->middlename = $withdrawal['middlename'];
        $model->withdrawal_email = $withdrawal['withdrawal_email'];
        $model->status = $withdrawal['status'];
        $model->contact_no = $withdrawal['contact_no'];
        $model->lipiemco_acct_no = $withdrawal['lipiemco_acct_no'];
        $model->amount_words = $withdrawal['amount_words'];
        $model->amount_figure = $withdrawal['amount_figure'];
        $model->mode_payment = $withdrawal['mode_payment'];
        $model->child_firstname = $withdrawal['child_firstname'];
        $model->child_lastname = $withdrawal['child_lastname'];
        $model->child_middlename = $withdrawal['child_middlename'];

        $model->save();

        return $model;
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }
    public function updateStatus($id, $status)
    {
        $model = SavingsWithdrawal::find($id);
        $model->status = $status;

        $model->save();
        return $model;
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }
}