<?php

namespace App\Providers;

use App\Repositories\BorrowerRepository;
use App\Repositories\BorrowerRepositoryInterface;
use App\Repositories\LoanRepository;
use App\Repositories\LoanRepositoryInterface;
use App\Repositories\SpouseRepository;
use App\Repositories\SpouseRepositoryInterface;
use App\Repositories\CoMakerRepository;
use App\Repositories\CoMakerRepositoryInterface;
use App\Repositories\ServiceRepository;
use App\Repositories\ServiceRepositoryInterface;
use Illuminate\Support\ServiceProvider;
use App\Repositories\SavingsWithdrawalRepository;
use App\Repositories\SavingsWithdrawalRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(BorrowerRepositoryInterface::class, BorrowerRepository::class);
        $this->app->bind(LoanRepositoryInterface::class, LoanRepository::class);
        $this->app->bind(SpouseRepositoryInterface::class, SpouseRepository::class);
        $this->app->bind(CoMakerRepositoryInterface::class, CoMakerRepository::class);
        $this->app->bind(ServiceRepositoryInterface::class, ServiceRepository::class);
        $this->app->bind(SavingsWithdrawalRepositoryInterface::class, SavingsWithdrawalRepository::class);
    }
}
