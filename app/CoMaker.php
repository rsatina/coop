<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoMaker extends Model
{
    public function borrower()
    {
        return $this->belongsTo('Borrower');
    }
}
