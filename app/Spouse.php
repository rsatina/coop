<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spouse extends Model
{
    public function borrower()
    {
        return $this->belongsTo('Borrower');
    }
}
