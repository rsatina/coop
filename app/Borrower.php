<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrower extends Model
{
    public function loan()
    {
        return $this->hasMany(Loan::class);
    }

    public function spouse()
    {
        return $this->hasOne(Spouse::class);
    }

    public function comaker()
    {
        return $this->hasMany(CoMaker::class);
    }
}
