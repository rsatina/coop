<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CoMakerRepositoryInterface;
use App\Repositories\BorrowersRepositoryInterface;
use App\Borrower;

class ComakerController extends Controller
{
    private $comakerRepository;
    public function __construct(
        CoMakerRepositoryInterface $comakerRepository
    )
    {
        $this->comakerRepository = $comakerRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showcomaker($id, Request $request)
    {
        $page = '';

        $com = $this->comakerRepository->getById($id);
        $borrower_id = $com->borrower_id;
        $comaker = $this->comakerRepository->getDetailsByBorrowerAndComakerId($id, $borrower_id);
        return view('comakers.showcomaker', compact('comaker', 'page'));
    }

    /**
     * @return mixed
     */
    public function comaker(Request $request)
    {
        $page = 'Co-Maker';

        $comakers = $this->comakerRepository->getComakerByEmail(auth()->user()->email);
        return view('comakers.comaker', compact('comakers', 'page'));
    }



    
}
