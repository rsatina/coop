<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\BorrowerRepositoryInterface;
use App\Repositories\LoanRepositoryInterface;
use App\Repositories\SpouseRepositoryInterface;
use App\Repositories\CoMakerRepositoryInterface;
use App\Repositories\SavingsWithdrawalRepositoryInterface;

class TransactionController extends Controller
{
    private $borrowerRepository;
    private $loanRepository;
    private $spouseRepository;
    private $comakerRepository;
    private $withdrawalRepository;

    public function __construct(
        BorrowerRepositoryInterface $borrowerRepository,
        LoanRepositoryInterface $loanRepository,
        SpouseRepositoryInterface $spouseRepository,
        CoMakerRepositoryInterface $comakerRepository,
        SavingsWithdrawalRepositoryInterface $withdrawalRepository
    )
    {
        $this->borrowerRepository = $borrowerRepository;
        $this->loanRepository = $loanRepository;
        $this->spouseRepository = $spouseRepository;
        $this->comakerRepository = $comakerRepository;
        $this->withdrawalRepository = $withdrawalRepository;
    }
    /**
     * @return mixed
     */
    public function transactions(Request $request)
    {
        $page = 'My Transactions';
        if (auth()->user()->isAdministrator())
        {
            $loans = $this->loanRepository->getAll();
            $withdrawals = $this->withdrawalRepository->getAll();
        } else {
            $loans = $this->loanRepository->getAllByUserId(auth()->user()->id);
            $withdrawals = $this->withdrawalRepository->getAllByUserId(auth()->user()->id);
        }
        $merge = $withdrawals->mergeRecursive($loans);

        $transactions = $merge->sortByDesc('created_at');

        return view('transactions.transactions', compact('transactions','page'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showtransaction(Request $request)
    {
        $page = 'Transaction List';

        return view('transactions.showtransaction', compact('page'));
    }


    
}
