<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\BorrowerRepositoryInterface;
use App\Repositories\LoanRepositoryInterface;
use App\Repositories\SavingsWithdrawalRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyTransaction;

class WithdrawalController extends Controller
{
    private $borrowerRepository;
    private $loanRepository;
    private $withdrawalRepository;

    public function __construct(
        BorrowerRepositoryInterface $borrowerRepository,
        LoanRepositoryInterface $loanRepository,
        SavingsWithdrawalRepositoryInterface $withdrawalRepository
    )
    {
        $this->borrowerRepository = $borrowerRepository;
        $this->loanRepository = $loanRepository;
        $this->withdrawalRepository = $withdrawalRepository;
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showsavingswithdrawal(Request $request)
    {
        $page = 'Savings Withdrawal';

        return view('withdrawals.showsavingswithdrawal', compact('page'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function savingswithdrawal(Request $request)
    {
        $page = 'Savings Withdrawal';

        $date = $request->get('date-slot', '');
        $time = $request->get('time-slot', '');
        $action = $request->get('action');

        if (!$date) {
            $date = now()->format('Y-m-d');
        }
        $withdrawals = [];
        if(auth()->user()->isAdministrator()) 
        {
            $withdrawals = $this->withdrawalRepository->getAll();
        }
        return view('withdrawals.savingswithdrawal', compact('withdrawals', 'date', 'time', 'page'));
    }

    public function storeWithdrawal(Request $request)
    {
        $date = Carbon::now()->format('Ymd');
        $savings_type = $request->get('savings_type');
        $withdrawal = $request->except([
            '_token'
        ]);
        if (!array_key_exists('child_firstname', $withdrawal)) {
            $withdrawal['child_firstname'] = null;
        }
        if (!array_key_exists('child_lastname', $withdrawal)) {
            $withdrawal['child_lastname'] = null;
        }
        if (!array_key_exists('child_middlename', $withdrawal)) {
            $withdrawal['child_middlename'] = null;
        }

        $withdrawal['status'] = 'For Verification';
        $w = $this->withdrawalRepository->getAll();
        if ($w->count() > 0) {
            $w_id = $w->first()->id + 1;
        } else {
            $w_id = 1;
        }
        $sequence_no = str_pad($w_id, 4, '0', STR_PAD_LEFT);

        if ($savings_type == 'Regular Savings') {
            $withdrawal['savings_withdrawal_no'] = 'RS' . $date . '-' . $sequence_no;
        } elseif ($savings_type == 'Special Savings') {
            $withdrawal['savings_withdrawal_no'] = 'SS' . $date . '-' . $sequence_no;
        } elseif ($savings_type == 'Kiddie Savings') {
            $withdrawal['savings_withdrawal_no'] = 'KS' . $date . '-' . $sequence_no;
        } else {
            $withdrawal['savings_withdrawal_no'] = null;
        }
        $withdrawal['user_id'] = auth()->user()->id;
        $withdrawal = $this->withdrawalRepository->add($withdrawal);

        $url = '/withdrawals/verifyWithdrawal/'.$withdrawal->id;
        $mailData = [
            'name' => $withdrawal->firstname,
            'transaction' => 'Savings Withdrawal',
            'url' => url($url)
        ];

        Mail::to($withdrawal->withdrawal_email)->send(new VerifyTransaction($mailData));
        $message = "Savings Withdrawal applied.  Please check your email ".$withdrawal->withdrawal_email." to verify your withdrawal application.";

        return redirect()->route('transactions.transactions')->with('message', $message);
    }

    public function verifyWithdrawal($id, Request $request)
    {
        $status = "Verified";
        $this->withdrawalRepository->updateStatus($id, $status);

        $message = 'Savings Withdrawal Verified successfully.  Withdrawal Status has been change from `For Verification` to `Verified`';
        return redirect()->route('transactions.transactions')->with('message', $message);
    }

    public function cancelWithdrawal($id)
    {
        $status = "Cancelled";
        $withdrawal = $this->withdrawalRepository->getById($id);
        if($withdrawal->status == "Cancelled") {
            $message = 'This transaction has already been cancelled.';
        } else {
            $message = 'Savings Withdrawal: '.$withdrawal->savings_withdrawal_no.' has been cancelled.';
            $this->withdrawalRepository->updateStatus($id, $status);
        }
        return redirect()->route('transactions.transactions')->with('message', $message);
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function processsavingswithdrawal(Request $request)
    {
        $page = 'Processing Savings Withdrawal';

        return view('withdrawals.processsavingswithdrawal', compact('page'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewsavingswithdrawal($id, Request $request)
    {
        $page = 'View Savings Withdrawal';
        $withdrawal = $this->withdrawalRepository->getById($id);

        return view('withdrawals.viewsavingswithdrawal', compact('withdrawal', 'page'));
    }


}
