<?php

namespace App\Http\Controllers;

use App\Repositories\BorrowerRepositoryInterface;
use App\Repositories\LoanRepositoryInterface;
use App\Repositories\SpouseRepositoryInterface;
use App\Repositories\CoMakerRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyTransaction;

class LoanController extends Controller
{
    private $borrowerRepository;
    private $loanRepository;
    private $spouseRepository;
    private $comakerRepository;

    public function __construct(
        BorrowerRepositoryInterface $borrowerRepository,
        LoanRepositoryInterface $loanRepository,
        SpouseRepositoryInterface $spouseRepository,
        CoMakerRepositoryInterface $comakerRepository
    )
    {
        $this->borrowerRepository = $borrowerRepository;
        $this->loanRepository = $loanRepository;
        $this->spouseRepository = $spouseRepository;
        $this->comakerRepository = $comakerRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $page = 'Loan Application';

        $date = $request->get('date-slot', '');
        $time = $request->get('time-slot', '');

        if (!$date) {
            $date = now()->format('Y-m-d');
        }

        $loans = [];
        if(auth()->user()->isAdministrator()) {
            $loans = $this->loanRepository->getAll();
        }
        return view('loans.create', compact('loans', 'date', 'time', 'page'));
    }

    public function store(Request $request)
    {
        $loan_type = $request->get('loan_type');
        $date = Carbon::now()->format('Ymd');

        $lipiemco_staff = $request->get('lipiemco');
        $property_owner = $request->get('property_owner');

        // Create borrower before we set loan, spouse, and co-maker
        $borrower = $request->only([
            'lipiemco',
            'borrower_last_name',
            'borrower_first_name',
            'borrower_middle_name',
            'borrower_present_address',
            'borrower_years_of_stay',
            'mode_of_stay',
            'property_owner',
            'provincial_address',
            'borrower_email',
            'borrower_contact_no',
            'borrower_employer_name',
            'borrower_position',
            'employment_status',
            'borrower_years_of_employment'
        ]);

        if ($lipiemco_staff == null) {
            $borrower['lipiemco'] = null;
        }

        if ($property_owner == null) {
            $borrower['property_owner'] = null;
        }

        $borrower['user_id'] = auth()->user()->id;
        $borrower = $this->borrowerRepository->add($borrower);

        // Create loan
        $loan = $request->only([
            'loan_type',
            'loan_date',
            'amount',
            'amount_applied',
            'purpose',
            'term_applied',
            'interest_rate',
            'mode_payment',
            'service_charge',
            'special_savings'
        ]);
        
        $sequence_no = str_pad($borrower->id, 4, '0', STR_PAD_LEFT);

        if ($loan_type == 'Providential Loan') {
            $loan['loan_application_no'] = 'PL' . $date . '-' . $sequence_no;
        } elseif ($loan_type == 'Express Loan') {
            $loan['loan_application_no'] = 'EL' . $date . '-' . $sequence_no;
        } elseif ($loan_type == 'Instant Loan') {
            $loan['loan_application_no'] = 'IL' . $date . '-' . $sequence_no;
        } elseif ($loan_type == 'Seasonal Loan') {
            $loan['loan_application_no'] = 'SL' . $date . '-' . $sequence_no;
        } elseif ($loan_type == 'Business Loan') {
            $loan['loan_application_no'] = 'BL' . $date . '-' . $sequence_no;
        }

        $loan['status'] = "For Verification";
        $loan['borrower_id'] = $borrower->id;
        $loan = $this->loanRepository->add($loan);

        // Create spouse
        $spouse = $request->only([
            'spouse_last_name',
            'spouse_first_name',
            'spouse_middle_name',
            'spouse_email',
            'spouse_contact_no',
            'spouse_employer_name',
            'spouse_position',
            'spouse_employment_status',
            'spouse_years_of_employment'
        ]);

        $spouse['borrower_id'] = $borrower->id;
        $spouse = $this->spouseRepository->add($spouse);

        // Create co-maker1
        $comaker1 = $request->only([
            'coMaker1_last_name',
            'coMaker1_first_name',
            'coMaker1_middle_name',
            'coMaker1_email',
            'coMaker1_contact_no',
            'coMaker1_employer_name',
            'coMaker1_position',
            'coMaker1_employment_status',
            'coMaker1_years_of_employment'
        ]);

        $comaker = [];
        $comaker['lastname'] = $comaker1['coMaker1_last_name'];
        $comaker['firstname'] = $comaker1['coMaker1_first_name'];
        $comaker['middlename'] = $comaker1['coMaker1_middle_name'];
        $comaker['comaker_email'] = $comaker1['coMaker1_email'];
        $comaker['contact_no'] = $comaker1['coMaker1_contact_no'];
        $comaker['employer_name'] = $comaker1['coMaker1_employer_name'];
        $comaker['position'] = $comaker1['coMaker1_position'];
        $comaker['employment_status'] = $comaker1['coMaker1_employment_status'];
        $comaker['yrs_employment'] = $comaker1['coMaker1_years_of_employment'];

        $comaker['borrower_id'] = $borrower->id;
        $this->comakerRepository->add($comaker);

        // Create co-maker2
        $comaker2 = $request->only([
            'coMaker2_last_name',
            'coMaker2_first_name',
            'coMaker2_middle_name',
            'coMaker2_email',
            'coMaker2_contact_no',
            'coMaker2_employer_name',
            'coMaker2_position',
            'coMaker2_employment_status',
            'coMaker2_years_of_employment'
        ]);

        $comaker = [];
        $comaker['lastname'] = $comaker2['coMaker2_last_name'];
        $comaker['firstname'] = $comaker2['coMaker2_first_name'];
        $comaker['middlename'] = $comaker2['coMaker2_middle_name'];
        $comaker['comaker_email'] = $comaker2['coMaker2_email'];
        $comaker['contact_no'] = $comaker2['coMaker2_contact_no'];
        $comaker['employer_name'] = $comaker2['coMaker2_employer_name'];
        $comaker['position'] = $comaker2['coMaker2_position'];
        $comaker['employment_status'] = $comaker2['coMaker2_employment_status'];
        $comaker['yrs_employment'] = $comaker2['coMaker2_years_of_employment'];

        $comaker['borrower_id'] = $borrower->id;
        $this->comakerRepository->add($comaker);

        $url = '/loans/verifyLoan/'.$loan->id;
        $mailData = [
            'name' => $borrower->firstname,
            'transaction' => 'Loan',
            'url' => url($url)
        ];

        Mail::to($borrower->borrower_email)->send(new VerifyTransaction($mailData));
        $message = "Loan Application created.  Please check your email ".$borrower->borrower_email." to verify your loan.";
        switch($request->input('action'))
        {
            case 'create_loan': {

                $request->session()->flash('message', $message);

                return redirect()->route('transactions.transactions');
            }
        }
    }

    public function cancelLoan($id)
    {
        $status = "Cancelled";
        $loan = $this->loanRepository->getById($id);
        if ($loan->status == "Cancelled") {
            $message = "This transaction has already been cancelled.";
        } else {
            $message = 'Loan Application: '.$loan->loan_application_no.' has been cancelled.';
            $this->loanRepository->updateStatus($id, $status);
        }
        return redirect()->route('transactions.transactions')->with('message', $message);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showloanapplication($id, Request $request)
    {
        $page = 'Loan Application';

        $loan = $this->loanRepository->getById($id);
        return view('loans.showloanapplication', compact('loan', 'page'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function evaluateloanapplication(Request $request)
    {
        $page = 'Loan Application Evaluation';

        return view('loans.evaluateloanapplication', compact('page'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reviewapprovalloanapplication(Request $request)
    {
        $page = 'Reviewing Loan Application';

        return view('loans.reviewapprovalloanapplication', compact('page'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewloanapplication($id, Request $request)
    {
        $page = 'View Loan Application';

        $loan = $this->loanRepository->getById($id);
        $borrower = $this->borrowerRepository->getById($loan->borrower_id);
        $spouse = $this->spouseRepository->getByBorrowerId($loan->borrower_id);
        $comaker = $this->comakerRepository->getByBorrowerId($loan->borrower_id);
        return view('loans.viewloanapplication', compact('loan','borrower', 'spouse', 'comaker', 'page'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function releaseloanapplication(Request $request)
    {
        $page = 'Releasing Loan Application';

        return view('loans.releaseloanapplication', compact('page'));
    }

    public function verifyLoan($id, Request $request)
    {
        $status = "Verified";
        $this->loanRepository->updateStatus($id, $status);

        $message = 'Loan Verified successfully.  Loan Status has been change from `For Verification` to `Verified`';
        return redirect()->route('transactions.transactions')->with('message', $message);
    }

}
