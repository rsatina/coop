<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CalendarEvents extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];

        $this->collection->each(function($calendarEvent) use (&$data) {

            $event = [
                'title' => $calendarEvent->schedule_time,
                'start' => $calendarEvent->schedule_date,
                'end' => $calendarEvent->schedule_end_date
            ];

            if ($calendarEvent->is_block)
            {
                $event['rendering'] = 'background';
                $event['backgroundColor'] = '#dddddd';
            }

            $data[] = $event;
        });

        return $data;
    }
}
