<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\VerifyLoan;

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Route::get('/refresh_captcha', 'Auth\RegisterController@refreshCaptcha');

Route::get('/passwordExpiration','Auth\PwdExpirationController@showPasswordExpirationForm');
Route::post('/passwordExpiration','Auth\PwdExpirationController@postPasswordExpiration')->name('passwordExpiration');

Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');

Auth::routes(['verify' => true]);

Route::middleware(['auth' => 'verified'])->group(function () {

    Route::prefix('loans')->group(function () {

        /* Loan Application Related Routes */
        Route::get('create', 'LoanController@create')->name('loans.create');
        Route::post('store', 'LoanController@store');
        Route::get('/', 'LoanController@create')->name('loans.create');
        Route::get('showloanapplication/{id}', 'LoanController@showloanapplication')->name('loans.showloanapplication');
        Route::get('evaluateloanapplication/{id}', 'LoanController@evaluateloanapplication')->name('loans.evaluateloanapplication');
        Route::get('reviewapprovalloanapplication/{id}', 'LoanController@reviewapprovalloanapplication')->name('loans.reviewapprovalloanapplication');
        Route::get('viewloanapplication/{id}', 'LoanController@viewloanapplication')->name('loans.transactions');
        Route::get('releaseloanapplication/{id}', 'LoanController@releaseloanapplication')->name('loans.releaseloanapplication');
        Route::get('verifyLoan/{id}', 'LoanController@verifyLoan')->name('loans.verifyLoan');
        Route::get('cancelLoan/{id}', 'LoanController@cancelLoan')->name('loans.cancelLoan');

        Route::get('pledgedepositform', function () {
            return view('loans.pledgedepositform');
        });

        Route::get('authoritydeductform', function () {
            return view('loans.authoritydeductform');
        });

        Route::get('promissorynoteform', function () {
            return view('loans.promissorynoteform');
        });

        Route::get('personaldisclosureform', function () {
            return view('loans.personaldisclosureform');
        });

    });
    Route::prefix('withdrawals')->group(function () {
        /* Withdrawal Related Routes */
        Route::get('savingswithdrawal', 'WithdrawalController@savingswithdrawal')->name('withdrawals.savingswithdrawal');
        Route::get('showsavingswithdrawal/{id}', 'WithdrawalController@showsavingswithdrawal')->name('withdrawals.showsavingswithdrawal');
        Route::get('processsavingswithdrawal/{id}', 'WithdrawalController@processsavingswithdrawal')->name('withdrawals.processsavingswithdrawal');
        Route::get('viewsavingswithdrawal/{id}', 'WithdrawalController@viewsavingswithdrawal')->name('withdrawals.transactions');
        Route::post('storeWithdrawal', 'WithdrawalController@storeWithdrawal');
        Route::get('verifyWithdrawal/{id}', 'WithdrawalController@verifyWithdrawal')->name('withdrawals.verifyWithdrawal');
        Route::get('cancelWithdrawal/{id}', 'WithdrawalController@cancelWithdrawal')->name('withdrawals.cancelWithdrawal');

    });
    Route::prefix('transactions')->group(function () {
        /* Transaction Related Routes */
        Route::get('transactions', 'TransactionController@transactions')->name('transactions.transactions');
        Route::get('showtransaction/{id}', 'TransactionController@showtransaction')->name('transactions.showtransaction');

    });
    Route::prefix('comakers')->group(function () {
        /* Comaker Related Routes */
        Route::get('comaker', 'ComakerController@comaker')->name('comakers.comaker');
        Route::get('showcomaker/{id}', 'ComakerController@showcomaker')->name('comakers.showcomaker');

    });

});