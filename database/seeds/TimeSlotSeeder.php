<?php

use Illuminate\Database\Seeder;

class TimeSlotSeeder extends Seeder
{
    protected $timeInterval = '00:30:00';

    protected $startDate = '2020-05-01';

    protected $endDate = '2020-06-30';

    protected $startTime = '09:00:00';

    protected $endTime = '18:00:00';

    protected $slotCount = 2;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datePeriod = \Carbon\CarbonPeriod::create($this->startDate, $this->endDate);

        foreach ($datePeriod as $date)
        {
            $slotDate = $date->format('Y-m-d');

            $timePeriod = \Carbon\CarbonInterval::minutes(30)->toPeriod($slotDate . ' ' . $this->startTime, $slotDate . ' ' . $this->endTime);

            foreach ($timePeriod as $time)
            {
                $slotTime = $time->format('H:i:s');

                $model = new \App\TimeSlot;

                $model->slot_date = $slotDate;
                $model->slot_time = $slotTime;
                $model->slot_count = $this->slotCount;

                $model->save();
            }
        }
    }
}
