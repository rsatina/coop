<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBorrowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('borrowers')) {
            Schema::create('borrowers', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('user_id')->unsigned();
                $table->string('firstname')->nullable();
                $table->string('lastname')->nullable();
                $table->string('middlename')->nullable();
                $table->string('present_address')->nullable();
                $table->integer('no_yrs_stay')->nullable();
                $table->string('place_of_residence')->nullable();
                $table->string('property_owner')->nullable();
                $table->string('lipiemco_staff')->nullable();
                $table->string('provincial_address')->nullable();
                $table->string('borrower_email');
                $table->string('contact_no')->nullable();
                $table->string('employer_name')->nullable();
                $table->string('position')->nullable();
                $table->string('employer_status')->nullable();
                $table->integer('yrs_employment')->nullable();
                $table->string('image')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrowers');
    }
}
