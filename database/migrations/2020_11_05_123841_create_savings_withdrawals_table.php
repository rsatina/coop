<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSavingsWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('savings_withdrawals')) {
            Schema::create('savings_withdrawals', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('user_id')->unsigned();
                $table->string('savings_withdrawal_no')->nullable();
                $table->string('savings_type')->nullable();
                $table->string('lastname')->nullable();
                $table->string('firstname')->nullable();
                $table->string('middlename')->nullable();
                $table->string('withdrawal_email');
                $table->string('contact_no')->nullable();
                $table->string('lipiemco_acct_no')->nullable();
                $table->integer('amount_figure')->nullable();
                $table->string('amount_words')->nullable();
                $table->string('mode_payment')->nullable();
                $table->string('child_lastname')->nullable();
                $table->string('child_firstname')->nullable();
                $table->string('child_middlename')->nullable();
                $table->string('status')->nullable();
                $table->string('message')->nullable();
                $table->timestamp('reviewed_at')->nullable();
                $table->timestamp('evaluated_at')->nullable();
                $table->timestamp('approved_at')->nullable();
                $table->timestamp('released_at')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('savings_withdrawals');
    }
}
