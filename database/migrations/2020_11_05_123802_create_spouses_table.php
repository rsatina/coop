<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('spouses')) {
            Schema::create('spouses', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('borrower_id');
                $table->string('lastname')->nullable();
                $table->string('firstname')->nullable();
                $table->string('middlename')->nullable();
                $table->string('spouse_email');
                $table->string('contact_no')->nullable();
                $table->string('employer_name')->nullable();
                $table->string('position')->nullable();
                $table->string('employment_status')->nullable();
                $table->integer('yrs_employment')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spouses');
    }
}
