<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('loans')) {
            Schema::create('loans', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('borrower_id');
                $table->string('loan_application_no')->nullable();
                $table->string('loan_type')->nullable();
                $table->string('amount_word')->nullable();
                $table->integer('amount_figure')->nullable();
                $table->integer('interest_rate')->nullable();
                $table->integer('service_charge')->nullable();
                $table->integer('special_savings')->nullable();
                $table->string('purpose')->nullable();
                $table->string('status')->nullable();
                $table->integer('term_applied')->nullable();
                $table->string('mode_of_payment')->nullable();
                $table->timestamp('reviewed_at')->nullable();
                $table->timestamp('evaluated_at')->nullable();
                $table->timestamp('approved_at')->nullable();
                $table->timestamp('released_at')->nullable();
                $table->string('message')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
