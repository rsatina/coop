const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableNotifications();

mix.copyDirectory('node_modules/bootstrap', 'public/vendor/bootstrap')
    .copyDirectory('node_modules/popper.js', 'public/vendor/popper')
    .copyDirectory('node_modules/jquery', 'public/vendor/jquery')
    .copyDirectory('node_modules/bootstrap-select/dist', 'public/vendor/bootstrap-select')
    .copyDirectory('node_modules/font-awesome', 'public/vendor/font-awesome')
    .copyDirectory('node_modules/moment', 'public/vendor/moment')
    .copyDirectory('node_modules/@fullcalendar', 'public/vendor/calendar');

