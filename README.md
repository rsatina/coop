## Clinic Web Appliccation
- Complete clinic web app that handles reservations, inventory and billing.

## Prerequesits
You must have these items installed and configured on your environment.  The Clinic App platform uses [Composer](https://getcomposer.org/) to pull in PHP dependencies and Node + NPM to pull in front-end dependencies.

1. All server requirements for Laravel 6.x must be met. [Laravel Server Requirements](https://laravel.com/docs/6.x#server-requirements)
2. You will need MySQL >= 5.7 installed.  You will need the ability to create a new database.  You can use the native mysql command line tool or use [XAMPP](https://www.apachefriends.org/download.html) tool to manage your mysql database.
3. You will need Node + NPM installed [Node](https://docs.npmjs.com/getting-started/installing-node).

## Installation

This process will install the Clinic App on your environment.

1. **Create the MySql database** : You can name the database whatever you like.  We recommend `clinic`.  You will need to modify the .env file in the next step. ( Note: There's a plan to include this in migration to create the database automatically during installation. But for now, let's do it manually.)
2. **Set your environment variables** : Create a file named `.env` in the root directory and copy the contents of `.env.example` into it.  Update any variables that relate to your environment.  You will most likely need to modify the database credentials to match your environment.
3. **Install and build**
      - Run `composer install` to pull in composer dependencies
      - Run `php artisan key:generate` to generate the APP_KEY
      - Run `php artisan migrate` to migrate the database
      - Run `php artisan db:seed` to seed the database
      - Run `npm install` to install third-party front end tools
      - Run `npm run dev` to build the front end assets.
      - Run `php artisan serve` to enable web server.  This is only true if you are not using XAMPP.
4. **Sign into the clinic app** : Navigate to your localhost url to view the login screen.

## Errors
1.  SQLSTATE[HY000] [2002] Connection refused
      - Check if your mysql service is running.  If not then start mysql service.
      - Set DB_HOST from .env file to 'localhost' instead of '127.0.0.1'
      - Add 'skip-grant-tables' in my.cnf file.
2.  `composer install` Downloading (failed)
      - Solution: `allow_url_fopen` must be enabled in php.ini
