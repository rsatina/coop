@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success text-dark">
                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                <i class="nc-icon nc-simple-remove"></i>
                            </button>
                            <span>
                                <b>
                                    {{ session('message') }}
                                </b>
                            </span>
                        </div>
                    @endif

                    @if($transactions || $withdrawals)
                    <div class="card table-with-links">
                        <div class="card-body table-responsive">
                        
                            @if ($errors->any())
                            <ul> 
                                @foreach ($errors->all() as $message)
                                    <li> {{ $message }}</li>
                                @endforeach
                            </ul>
                            @endif

                            <table class="table table-hover table-striped table-bordered">
                                <thead>
                                <tr class="d-flex" style="background-color:#182370">
                                    <th class="text-center col-2 text-white"><strong>Transaction Number</strong></th>
                                    <th class="text-center col-2 text-white"><strong>Date Submitted</strong></th>
                                    <th class="text-center col-1 text-white"><strong>Transaction Type</strong></th>
                                    <th class="text-center col-1 text-white"><strong>Amount Applied</strong></th>
                                    <th class="text-center col-1 text-white"><strong>Amount Release</strong></th>
                                    <th class="text-center col-2 text-white"><strong>Release Date</strong></th>
                                    <th class="text-center col-2 text-white"><strong>Status</strong></th>
                                    <th class="text-center col-1 text-white"><strong>Actions</strong></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($transactions as $key => $transaction)
                                    <tr class="d-flex">
                                        <form method="post" action="{{ url('transactions/status', ['id' => $transaction->id]) }}">
                                        @csrf
                                            <td class="text-center col-2">{{ $transaction->loan_application_no }}{{$transaction->savings_withdrawal_no}}</td>
                                            <td class="text-center col-2">{{ date('M-d-Y h:i A', strtotime($transaction->created_at)) }}</td>
                                            <td class="text-center col-1">{{ $transaction->loan_type }}{{ $transaction->savings_type }}</td>
                                            <td class="text-center col-1">{{ $transaction->amount_figure }}</td>
                                            <td class="text-center col-1"></td>
                                            <td class="text-center col-2"></td>
                                            <td class="text-center col-2">
                                                @if(auth()->user()->isAdministrator())
                                                    <select name="status" onchange='this.form.submit()' style="border:none;color:#888888" class="text-body">
                                                        <option value='1'>For Verification</option>
                                                        <option value='2'>Verified</option>
                                                        <option value='3'>For Evaluation</option>
                                                        <option value='4'>Loans Head Review</option>
                                                        <option value='5'>GM Approval</option>
                                                        <option value='5'>CreCom Approval</option>
                                                        <option value='5'>BOD Approval</option>
                                                        <option value='5'>Cashier Disbursement</option>
                                                        <option value='5'>Released/Deposited</option>
                                                        <option value='5'>Lacking Requirements</option>
                                                        <option value='5'>On-Hold</option>
                                                        <option value='5'>Cancelled</option>
                                                        <option value='5'>Denied</option>
                                                    </select>
                                                @else
                                                    {{ $transaction->status }}
                                                @endif
                                            </td>
                                            <td class="text-left col-1 td-actions" style="font-size:12px">
                                                @if($transaction->savings_type == "Regular Savings" ||
                                                    $transaction->savings_type == "Special Savings" ||
                                                    $transaction->savings_type == "Kiddie Savings")
                                                    <a href="{{ url('withdrawals/viewsavingswithdrawal', ['id' => $transaction->id]) }}">View</a><br>
                                                <a href="{{ url('withdrawals/cancelWithdrawal', ['id' => $transaction->id]) }}">Cancel</a><br>
                                                <a href="">Send a Message</a>
                                                @else
                                                    <a href="{{ url('loans/viewloanapplication', ['id' => $transaction->id]) }}">View</a><br>
                                                <a href="{{ url('loans/cancelLoan', ['id' => $transaction->id]) }}">Cancel</a><br>
                                                <a href="">Send a Message</a>
                                                @endif
                                            </td>
                                        </form>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @else
                    <div class="card">
                        <div class="card-body">
                            No Transactions. 
                            <p><a href="{{ url('loans/create') }}">Click here to apply for a loan!</a></p>
                            <p><a href="{{ url('withdrawals/savingswithdrawal') }}">Click here to apply for savings withdrawal!</a></p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


@section('javascript')
    <script type="text/javascript">
    jQuery.fn.extend({
      autoHeight: function () {
        function autoHeight_(element) {
          return jQuery(element).css({
            'height': 'auto',
            'overflow-y': 'hidden'
          }).height(element.scrollHeight);
        }
        return this.each(function () {
          autoHeight_(this).on('input', function () {
            autoHeight_(this);
          });
        });
      }
    });
    $('#covid_form').autoHeight();
    </script>
@endsection