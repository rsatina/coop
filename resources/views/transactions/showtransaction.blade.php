@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="">
                        @csrf
                        <div class="card">
                            @if ($errors)
                                <div class="card-header">
                                    <p class="error" style="color:red">{{ $errors->first() }}</p>
                                </div>
                            @endif
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="loan_type" class="text-body">Type of Loan <star class="star">*</star></label>
                                        <div class="row" id="loan_type">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="loan_type[]" value="Providential Loan">
                                                    <span class="form-radio-sign"></span>
                                                    Providential Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="loan_type[]" value="Express Loan">
                                                    <span class="form-radio-sign"></span>
                                                    Express Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="loan_type[]" value="Instant Loan">
                                                    <span class="form-radio-sign"></span>
                                                    Instant Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="loan_type[]" value="Seasonal Loan">
                                                    <span class="form-radio-sign"></span>
                                                    Seasonal Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="loan_type[]" value="Business Loan">
                                                    <span class="form-radio-sign"></span>
                                                    Business Loan
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-body">Date </label>
                                            <input type="text" placeholder="Date" name="loan_date" class="form-control @error('loan_date') is-invalid @enderror">
                                            @error('loan_date')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Loan Application No. </label>
                                            <input type="text" placeholder="Loan Application No." name="loan_application_no" class="form-control @error('loan_application_no') is-invalid @enderror">
                                            @error('loan_application_no')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Amount Applied <star class="star">*</star></label>
                                            <input type="text" placeholder="Amount Applied" name="amount_applied" class="form-control @error('amount_applied') is-invalid @enderror">
                                            @error('amount_applied')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Php <star class="star">*</star></label>
                                            <input type="number" placeholder="Amount" name="amount" class="form-control @error('amount') is-invalid @enderror">
                                            @error('amount')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Purpose <star class="star">*</star></label>
                                            <input type="text" placeholder="Purpose" name="purpose" class="form-control @error('purpose') is-invalid @enderror">
                                            @error('purpose')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Interest Rate <star class="star">*</star></label>
                                            <input type="number" placeholder="Interest Rate" name="interest_rate" class="form-control @error('interest_rate') is-invalid @enderror">
                                            @error('interest_rate')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="text-body">Term Applied (months) <star class="star">*</star></label>
                                            <input type="number" placeholder="Term Applied" name="term_applied" class="form-control @error('term_applied') is-invalid @enderror">
                                            @error('term_applied')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="payment_mode" class="text-body">Mode of Payment <star class="star">*</star></label>
                                        <div class="row" id="payment_mode">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_mode_post_dated" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Post Dated Check
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_mode_payroll_deduction" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Payroll Deduction
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_mode_cash" value="Contractual">
                                                    <span class="form-radio-sign"></span>
                                                    Cash
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <!-- =============== Member Borrower's Details =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Member Borrower's Details</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('borrower_last_name', app('request')->input('borrower_last_name')) }}" name="borrower_last_name" class="form-control @error('borrower_last_name') is-invalid @enderror" required>
                                            @error('borrower_last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('borrower_first_name', app('request')->input('borrower_first_name')) }}" name="borrower_first_name" class="form-control @error('borrower_first_name') is-invalid @enderror" required>
                                            @error('borrower_first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('borrower_middle_name', app('request')->input('borrower_middle_name')) }}" name="borrower_middle_name" class="form-control @error('borrower_middle_name') is-invalid @enderror">
                                            @error('borrower_middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Present Address <star class="star">*</star></label>
                                            <input type="text" placeholder="Present Address" value="{{ old('borrower_present_address', app('request')->input('borrower_present_address')) }}" name="borrower_present_address" class="form-control @error('borrower_present_address') is-invalid @enderror" required>
                                            @error('borrower_present_address')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">No. of Years of Stay <star class="star">*</star></label>
                                            <input type="number" placeholder="No. of Years of Stay" name="borrower_years_of_stay" class="form-control @error('borrower_years_of_stay') is-invalid @enderror" required>
                                            @error('borrower_years_of_stay')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="row" id="mode_of_stay">
                                        <div class="form-check checkbox-inline">
                                            <label class="form-check-label text-body">
                                                <input class="form-radio-input" type="radio" name="mode_of_stay[]" value="Owned">
                                                <span class="form-radio-sign"></span>
                                                Owned
                                            </label>
                                        </div>
                                        <div class="form-check checkbox-inline">
                                            <label class="form-check-label text-body">
                                                <input class="form-radio-input" type="radio" name="mode_of_stay[]" value="Living with Parents/Relatives">
                                                <span class="form-radio-sign"></span>
                                                Living with Parents/Relatives
                                            </label>
                                        </div>
                                        <div class="form-check checkbox-inline">
                                            <label class="form-check-label text-body">
                                                <input class="form-radio-input" type="radio" name="mode_of_stay[]" value="Renting">
                                                <span class="form-radio-sign"></span>
                                                Renting
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="row" id="lipiemco_staff">
                                        <div class="form-check checkbox-inline">
                                            <label class="form-check-label text-body">
                                                <input class="form-radio-input" type="radio" name="lipiemco_staff[]" value="LIPIEMCO Employee">
                                                <span class="form-radio-sign"></span>
                                                LIPIEMCO Employee
                                            </label>
                                        </div>
                                        <div class="form-check checkbox-inline">
                                            <label class="form-check-label text-body">
                                                <input class="form-radio-input" type="radio" name="lipiemco_staff[]" value="LIPIEMCO Officer">
                                                <span class="form-radio-sign"></span>
                                                LIPIEMCO Officer
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">Provincial Address <star class="star">*</star></label>
                                            <input type="text" placeholder="Provincial Address" name="provincial_address" class="form-control @error('provincial_address') is-invalid @enderror">
                                            @error('provincial_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="borrower_email_address" class="form-control @error('borrower_email_address') is-invalid @enderror">
                                            @error('borrower_email_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="borrower_contact_no" class="form-control @error('borrower_contact_no') is-invalid @enderror">
                                            @error('borrower_contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Name of Employer <star class="star">*</star></label>
                                            <input type="email" placeholder="Name of Employer" name="borrower_employer_name" class="form-control @error('borrower_employer_name') is-invalid @enderror">
                                            @error('borrower_employer_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Position <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" name="borrower_position" class="form-control @error('borrower_position') is-invalid @enderror">
                                            @error('borrower_position')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="employment_status" class="text-body">Employment Status <star class="star">*</star></label>
                                        <div class="row" id="employment_status">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="employment_status[]" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Probitionary
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="employment_status[]" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Regular
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="employment_status[]" value="Contractual">
                                                    <span class="form-radio-sign"></span>
                                                    Contractual
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="employment_status[]" value="Project-Based">
                                                    <span class="form-radio-sign"></span>
                                                    Project-Based
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="text-body">Years of Employment <star class="star">*</star></label>
                                            <input type="number" placeholder="Years of Employment" name="borrower_years_of_employment" class="form-control @error('borrower_years_of_employment') is-invalid @enderror">
                                            @error('borrower_years_of_employment')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <!-- =============== SPOUSE DETAILS =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Spouse's Details</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('spouse_last_name', app('request')->input('spouse_last_name')) }}" name="spouse_last_name" class="form-control @error('spouse_last_name') is-invalid @enderror" required>
                                            @error('spouse_last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('spouse_first_name', app('request')->input('spouse_first_name')) }}" name="spouse_first_name" class="form-control @error('spouse_first_name') is-invalid @enderror" required>
                                            @error('spouse_first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('spouse_middle_name', app('request')->input('spouse_middle_name')) }}" name="spouse_middle_name" class="form-control @error('spouse_middle_name') is-invalid @enderror">
                                            @error('spouse_middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="spouse_email_address" class="form-control @error('spouse_email_address') is-invalid @enderror">
                                            @error('spouse_email_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="spouse_contact_no" class="form-control @error('spouse_contact_no') is-invalid @enderror">
                                            @error('spouse_contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Name of Employer <star class="star">*</star></label>
                                            <input type="email" placeholder="Name of Employer" name="spouse_employer_name" class="form-control @error('spouse_employer_name') is-invalid @enderror">
                                            @error('spouse_employer_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Position <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" name="spouse_position" class="form-control @error('spouse_position') is-invalid @enderror">
                                            @error('spouse_position')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="spouse_employment_status" class="text-body">Employment Status <star class="star">*</star></label>
                                        <div class="row" id="spouse_employment_status">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="spouse_employment_status[]" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Probitionary
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="spouse_employment_status[]" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Regular
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="spouse_employment_status[]" value="Contractual">
                                                    <span class="form-radio-sign"></span>
                                                    Contractual
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="spouse_employment_status[]" value="Project-Based">
                                                    <span class="form-radio-sign"></span>
                                                    Project-Based
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="text-body">Years of Employment <star class="star">*</star></label>
                                            <input type="number" placeholder="Years of Employment" name="spouse_years_of_employment" class="form-control @error('spouse_years_of_employment') is-invalid @enderror">
                                            @error('spouse_years_of_employment')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <!-- =============== Co-Maker Details (1) =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Co-Maker Details (1)</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('coMaker1_last_name', app('request')->input('coMaker1_last_name')) }}" name="coMaker1_last_name" class="form-control @error('coMaker1_last_name') is-invalid @enderror" required>
                                            @error('coMaker1_last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('coMaker1_first_name', app('request')->input('coMaker1_first_name')) }}" name="coMaker1_first_name" class="form-control @error('coMaker1_first_name') is-invalid @enderror" required>
                                            @error('coMaker1_first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('coMaker1_middle_name', app('request')->input('coMaker1_middle_name')) }}" name="coMaker1_middle_name" class="form-control @error('coMaker1_middle_name') is-invalid @enderror">
                                            @error('coMaker1_middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="coMaker1_email_address" class="form-control @error('coMaker1_email_address') is-invalid @enderror">
                                            @error('coMaker1_email_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="coMaker1_contact_no" class="form-control @error('coMaker1_contact_no') is-invalid @enderror">
                                            @error('coMaker1_contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Name of Employer <star class="star">*</star></label>
                                            <input type="email" placeholder="Name of Employer" name="coMaker1_employer_name" class="form-control @error('coMaker1_employer_name') is-invalid @enderror">
                                            @error('coMaker1_employer_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Position <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" name="coMaker1_position" class="form-control @error('coMaker1_position') is-invalid @enderror">
                                            @error('coMaker1_position')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="coMaker1_employment_status" class="text-body">Employment Status <star class="star">*</star></label>
                                        <div class="row" id="coMaker1_employment_status">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker1_employment_status[]" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Probitionary
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker1_employment_status[]" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Regular
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker1_employment_status[]" value="Contractual">
                                                    <span class="form-radio-sign"></span>
                                                    Contractual
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker1_employment_status[]" value="Project-Based">
                                                    <span class="form-radio-sign"></span>
                                                    Project-Based
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="text-body">Years of Employment <star class="star">*</star></label>
                                            <input type="number" placeholder="Years of Employment" name="coMaker1_years_of_employment" class="form-control @error('coMaker1_years_of_employment') is-invalid @enderror">
                                            @error('coMaker1_years_of_employment')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <!-- =============== Co-Maker Details (2) =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Co-Maker Details (2)</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('coMaker2_last_name', app('request')->input('coMaker2_last_name')) }}" name="coMaker2_last_name" class="form-control @error('coMaker2_last_name') is-invalid @enderror" required>
                                            @error('coMaker2_last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('coMaker2_first_name', app('request')->input('coMaker2_first_name')) }}" name="coMaker2_first_name" class="form-control @error('coMaker2_first_name') is-invalid @enderror" required>
                                            @error('coMaker2_first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('coMaker2_middle_name', app('request')->input('coMaker2_middle_name')) }}" name="coMaker2_middle_name" class="form-control @error('coMaker2_middle_name') is-invalid @enderror">
                                            @error('coMaker2_middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="coMaker2_email_address" class="form-control @error('coMaker2_email_address') is-invalid @enderror">
                                            @error('coMaker2_email_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="coMaker2_contact_no" class="form-control @error('coMaker2_contact_no') is-invalid @enderror">
                                            @error('coMaker2_contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Name of Employer <star class="star">*</star></label>
                                            <input type="email" placeholder="Name of Employer" name="coMaker2_employer_name" class="form-control @error('coMaker2_employer_name') is-invalid @enderror">
                                            @error('coMaker2_employer_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Position <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" name="coMaker2_position" class="form-control @error('coMaker2_position') is-invalid @enderror">
                                            @error('coMaker2_position')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="coMaker2_employment_status" class="text-body">Employment Status <star class="star">*</star></label>
                                        <div class="row" id="coMaker2_employment_status">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker2_employment_status[]" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Probitionary
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker2_employment_status[]" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Regular
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker2_employment_status[]" value="Contractual">
                                                    <span class="form-radio-sign"></span>
                                                    Contractual
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker2_employment_status[]" value="Project-Based">
                                                    <span class="form-radio-sign"></span>
                                                    Project-Based
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="text-body">Years of Employment <star class="star">*</star></label>
                                            <input type="number" placeholder="Years of Employment" name="coMaker2_years_of_employment" class="form-control @error('coMaker2_years_of_employment') is-invalid @enderror">
                                            @error('coMaker2_years_of_employment')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <h4 class="card-title">Attached Files</h4>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-6" >
                                        <div class="card w-75 h-100  border border-danger" style="width: 18rem; background-color: #ffffb3">
                                            <div class="text-center font-weight-bold mt-2"> Required Forms</div>
                                            <div class="text-center">
                                                @if(session()->has('screeningFormData'))
                                                    <a class="btn" href="{{ url ('transactions/personaldisclosureform') }}" data-form-screen style="border:none; background-color: #ffffb3; color:green">
                                                        <strong>Personal Data Disclosure Authorization Form</strong>        
                                                    </a><i class="fa fa-check-square-o"></i>
                                                @else
                                                    <a class="btn" href="{{ url ('transactions/personaldisclosureform') }}" data-form-screen style="border:none; background-color: #ffffb3; color:blue">
                                                        <strong>Personal Data Disclosure Authorization Form</strong>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="text-center">
                                                @if(session()->has('consentFormData'))
                                                    <a class="btn" href="{{ url('transactions/pledgedepositform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                        <strong>Pledge of Deposit Form</strong>
                                                    </a><i class="fa fa-check-square-o"></i>
                                                @else
                                                    <a class="btn" href="{{ url('transactions/pledgedepositform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                        <strong>Pledge of Deposit Form</strong>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="text-center">
                                                @if(session()->has('consentFormData'))
                                                    <a class="btn" href="{{ url('transactions/authoritydeductform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                        <strong>Authority to Deduct Form</strong>
                                                    </a><i class="fa fa-check-square-o"></i>
                                                @else
                                                    <a class="btn" href="{{ url('transactions/authoritydeductform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                        <strong>Authority to Deduct Form</strong>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="text-center">
                                                @if(session()->has('consentFormData'))
                                                    <a class="btn" href="{{ url('transactions/promissorynoteform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                        <strong>Promissory Note Form</strong>
                                                    </a><i class="fa fa-check-square-o"></i>
                                                @else
                                                    <a class="btn" href="{{ url('transactions/promissorynoteform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                        <strong>Promissory Note Form</strong>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                            <div class="card-footer ">
                                <button type="button" class="btn btn-fill btn-primary" disable>Submit</button>
                                <a href="{{ url('transactions/transactions') }}" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {

            
        });
    </script>

@endsection