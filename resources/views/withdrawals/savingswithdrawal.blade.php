@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        @if ($errors)
                            <div class="card-header">
                                <p class="error" style="color:red">{{ $errors->first() }}</p>
                            </div>
                        @endif
                        
                        @if(auth()->user()->isAdministrator())
                            <div class="table-with-links">
                                <div class="card-body table-bordered table-striped table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr class="d-flex" style="background-color:#182370">
                                                <th class="text-center col-2 text-white"><strong>Withdrawal Number</strong></th>
                                                <th class="text-center col-2 text-white"><strong>Date Submitted</strong></th>
                                                <th class="text-center col-2 text-white"><strong>Full Name</strong></th>
                                                <th class="text-center col-2 text-white"><strong>Savings Type</strong></th>
                                                <th class="text-center col-1 text-white"><strong>Amount</strong></th>
                                                <th class="text-center col-2 text-white"><strong>Status</strong></th>
                                                <th class="text-center col-1 text-white"><strong>Action</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($withdrawals as $key => $withdrawal)
                                            <tr class="d-flex">
                                                <td class="text-center col-2">{{ $withdrawal->savings_withdrawal_no }}</td>
                                                <td class="text-center col-2">{{ $withdrawal->created_at }}</td>
                                                <td class="text-center col-2">{{ $withdrawal->firstname }} {{ $withdrawal->lastname }}</td>
                                                <td class="text-center col-2">{{ $withdrawal->savings_type }}</td>
                                                <td class="text-center col-1">{{ $withdrawal->amount_figure }}</td>
                                                <td class="text-center col-2">{{ $withdrawal->status }}</td>
                                                <td  class="col-1">
                                                    <a href="{{ url('withdrawals/processsavingswithdrawal', ['id' => 1]) }}">- Process</a><br>
                                                    <a href="" >- For Approval</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                        <form method="POST" action="{{ url('withdrawals/storeWithdrawal') }}">
                            @csrf
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Date </label>
                                            <div class='input-group date' id='loan-date'>
                                                <input type='text' id="withdrawal_date" name="withdrawal_date" class="form-control datepicker @error('withdrawal_date') is-invalid @enderror" placeholder="YYYY-MM-DD" required/>
                                                <label class="input-group-append input-group-text" for="withdrawal_date" style="margin:inherit;border-radius:1px;">
                                                    <span class="fa fa-calendar"></span>
                                                </label>
                                            </div>
                                            @error('withdrawal_date')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body" for=>Savings Withdrawal No:</label>
                                            <input type="text" name="savings_withdrawal_no" style="border-style: none none none none;"  disabled>
                                            @error('loan_application_no')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <!-- =============== Savings Withdrawal =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Savings Withdrawal</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="savings_type" class="text-body">Type of Savings <star class="star">*</star></label>
                                        <div class="row" id="savings_type">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="savings_type" id="rs" value="Regular Savings">
                                                    <span class="form-radio-sign"></span>
                                                    Regular Savings
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="savings_type" id="ss" value="Special Savings">
                                                    <span class="form-radio-sign"></span>
                                                    Special Savings
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="savings_type" id="ks"value="Kiddie Savings">
                                                    <span class="form-radio-sign"></span>
                                                    Kiddie Savings
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('lastname') }}" name="lastname" class="form-control @error('lastname') is-invalid @enderror" required>
                                            @error('lastname')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('firstname') }}" name="firstname" class="form-control @error('firstname') is-invalid @enderror" required>
                                            @error('firstname')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('middlename') }}" name="middlename" class="form-control @error('middlename') is-invalid @enderror" required>
                                            @error('middlename')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="withdrawal_email" class="form-control @error('withdrawal_email') is-invalid @enderror" required>
                                            @error('withdrawal_email')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="contact_no" class="form-control @error('contact_no') is-invalid @enderror" required>
                                            @error('contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">LIPIEMCO Account No. <star class="star">*</star></label>
                                            <input type="number" placeholder="LIPIEMCO Account No." name="lipiemco_acct_no" class="form-control @error('lipiemco_acct_no') is-invalid @enderror" required>
                                            @error('lipiemco_acct_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Amount <star class="star">*</star></label>
                                            <input type="text" placeholder="Amount in words" id="words" name="amount_words" class="form-control @error('amount_words') is-invalid @enderror" readonly>
                                            @error('amount_words')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Php <star class="star">*</star></label>
                                            <input type="number" placeholder="Amount in Figure" id="number" name="amount_figure" class="form-control @error('amount_figure') is-invalid @enderror" required>
                                            @error('amount_figure')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="mode_payment" class="text-body">Options <star class="star">*</star></label>
                                        <div class="form-check checkbox-inline ml-2">
                                            <label class="text-body">
                                                <input class="form-radio-input" type="radio" name="mode_payment" value="Cash">
                                                <span class="form-radio-sign"></span>
                                                Cash
                                            </label>
                                        </div>
                                        <div class="form-check checkbox-inline ml-5">
                                            <label class="text-body">
                                                <input class="form-radio-input" type="radio" name="mode_payment" value="Cheque">
                                                <span class="form-radio-sign"></span>
                                                Cheque
                                            </label>
                                        </div>
                                        <div class="form-check pl-5 ml-5">
                                            <label class="text-body">
                                                <input class="form-radio-input" type="radio" name="mode_payment" value="Unionbank Direct Deposit">
                                                <span class="form-radio-sign"></span>
                                                Unionbank Direct Deposit
                                                <input class="form-check-input ml-2" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;width:150px" type="text" readonly>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-check checkbox-inline" style="margin-left: 8rem;">
                                            <label class="text-body">
                                                <input class="form-radio-input" type="radio" name="mode_payment" value="GCash">
                                                <span class="form-radio-sign"></span>
                                                GCash
                                                <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;width:230px" type="text" readonly>
                                            </label>
                                        </div>
                                        <div class="form-check checkbox" style="margin-left: 8rem;">
                                            <label class="text-body">
                                                <input class="form-radio-input" type="radio" name="mode_payment" value="BPI Direct Deposit">
                                                <span class="form-radio-sign"></span>
                                                BPI Direct Deposit
                                                <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;width:150px" type="text" readonly>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <p id="kiddie_savings_label"><strong>Kiddie Savings Details</strong></p>
                                <div class="row" id="kiddie_savings">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Child's Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Name of Employer" id="child_lastname" name="child_lastname" class="form-control @error('child_lastname') is-invalid @enderror" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Child's First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" id="child_firstname" name="child_firstname" class="form-control @error('child_firstname') is-invalid @enderror" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Child's Middle Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" id="child_middlename" name="child_middlename" class="form-control @error('child_middlename') is-invalid @enderror" required>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <!-- =============== Authorization to Credit Bank Account =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Authority to Credit to My Bank Account</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group font-weight-normal">
                                            <p class="text-justify">I hereby authorize LIPIEMCO to directly credit to my <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width: 500px;"> account the withdrawn
                                            amount from my savings deposit. Such crediting to my bank/ATM/GCash account signifies my
                                            receipt of the said withdrawal transaction.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-fill btn-primary">Submit</button>
                                <a href="{{ url()->previous() }}" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            // date today
            var date = new Date().toISOString().slice(0,10);

            // Date picker set
            var loandatePicker = $("#withdrawal_date");

            loandatePicker.datetimepicker({
                viewMode: "years",
                format: 'YYYY-MM-DD',
                minDate: moment().format("YYYY-MM-DD")
            });

            $("#withdrawal_date").val(date);
            
            $('#kiddie_savings').hide();
            $('#kiddie_savings_label').hide();
        });

        const arr = x => Array.from(x);
        const num = x => Number(x) || 0;
        const str = x => String(x);
        const isEmpty = xs => xs.length === 0;
        const take = n => xs => xs.slice(0,n);
        const drop = n => xs => xs.slice(n);
        const reverse = xs => xs.slice(0).reverse();
        const comp = f => g => x => f (g (x));
        const not = x => !x;
        const chunk = n => xs =>
        isEmpty(xs) ? [] : [take(n)(xs), ...chunk (n) (drop (n) (xs))];

        // numToWords :: (Number a, String a) => a -> String
        let numToWords = n => {
        
            let a = [
                '', 'One', 'Two', 'Three', 'Four',
                'Five', 'Six', 'Seven', 'Eight', 'Nine',
                'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen',
                'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'
            ];
            
            let b = [
                '', '', 'Twenty', 'Thirty', 'Forty',
                'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'
            ];
            
            let g = [
                '', 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion',
                'Quintillion', 'Sixtillion', 'Septillion', 'Octillion', 'Nonillion'
            ];
            
            // this part is really nasty still
            // it might edit this again later to show how Monoids could fix this up
            let makeGroup = ([ones,tens,huns]) => {
                return [
                num(huns) === 0 ? '' : a[huns] + ' Hundred ',
                num(ones) === 0 ? b[tens] : b[tens] && b[tens] + '-' || '',
                a[tens+ones] || a[ones]
                ].join('');
            };
            
            let thousand = (group,i) => group === '' ? group : `${group} ${g[i]}`;
            
            if (typeof n === 'number')
                return numToWords(String(n));
            else if (n === '0')
                return 'Zero';
            else
                return comp (chunk(3)) (reverse) (arr(n))
                .map(makeGroup)
                .map(thousand)
                .filter(comp(not)(isEmpty))
                .reverse()
                .join(' ');
        };

        document.getElementById('number').onkeyup = function () {
            $words_value = numToWords(document.getElementById('number').value);

            $('#words').val($words_value);
        };

        $('#rs').click(function(){
            $('#kiddie_savings').hide();
            $('#kiddie_savings_label').hide();
            $('#child_firstname').attr('disabled', 'disabled');
            $('#child_lastname').attr('disabled', 'disabled');
            $('#child_middlename').attr('disabled', 'disabled');
        })
        $('#ss').click(function(){
            $('#kiddie_savings').hide();
            $('#kiddie_savings_label').hide();
            $('#child_firstname').attr('disabled', 'disabled');
            $('#child_lastname').attr('disabled', 'disabled');
            $('#child_middlename').attr('disabled', 'disabled');
        })
        $('#ks').click(function(){
            $('#kiddie_savings').show();
            $('#kiddie_savings_label').show();
            $('#child_firstname').removeAttr('disabled');
            $('#child_lastname').removeAttr('disabled');
            $('#child_middlename').removeAttr('disabled');
        })
    </script>

@endsection