@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="">
                        @csrf
                        <div class="card">
                            @if ($errors)
                                <div class="card-header">
                                    <p class="error" style="color:red">{{ $errors->first() }}</p>
                                </div>
                            @endif
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Savings Withdrawal No. </label>
                                            <input type="text" placeholder="Loan Application No." name="savings_withdrawal_no" class="form-control @error('savings_withdrawal_no') is-invalid @enderror" disabled>
                                            @error('savings_withdrawal_no')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Date </label>
                                            <div class='input-group date' id='loan-date'>
                                                <input type='text' id="loandate-picker" class="form-control datepicker @error('loandate-picker') is-invalid @enderror" name="loan_date" placeholder="YYYY-MM-DD" required/>
                                                <label class="input-group-append input-group-text" for="loandate-picker" style="margin:inherit;border-radius:1px;">
                                                    <span class="fa fa-calendar"></span>
                                                </label>
                                            </div>
                                            @error('loan_date')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <!-- =============== Savings Withdrawal =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Savings Withdrawal</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('borrower_last_name', app('request')->input('borrower_last_name')) }}" name="borrower_last_name" class="form-control @error('borrower_last_name') is-invalid @enderror" required>
                                            @error('borrower_last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('borrower_first_name', app('request')->input('borrower_first_name')) }}" name="borrower_first_name" class="form-control @error('borrower_first_name') is-invalid @enderror" required>
                                            @error('borrower_first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('borrower_middle_name', app('request')->input('borrower_middle_name')) }}" name="borrower_middle_name" class="form-control @error('borrower_middle_name') is-invalid @enderror">
                                            @error('borrower_middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="borrower_email_address" class="form-control @error('borrower_email_address') is-invalid @enderror">
                                            @error('borrower_email_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="borrower_contact_no" class="form-control @error('borrower_contact_no') is-invalid @enderror">
                                            @error('borrower_contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">LIPIEMCO Account No. <star class="star">*</star></label>
                                            <input type="number" placeholder="LIPIEMCO Account No." name="lipiemco_account_no" class="form-control @error('lipiemco_account_no') is-invalid @enderror">
                                            @error('lipiemco_account_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Php <star class="star">*</star></label>
                                            <input type="number" placeholder="Amount in Figure" name="amount_figure" class="form-control @error('amount_figure') is-invalid @enderror">
                                            @error('amount_figure')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Amount <star class="star">*</star></label>
                                            <input type="text" placeholder="Amount in words" name="amount_words" class="form-control @error('amount_words') is-invalid @enderror">
                                            @error('amount_words')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="payment_options" class="text-body">Options <star class="star">*</star></label>
                                        <div class="row" id="payment_options">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="payment_options" value="Cash">
                                                    <span class="form-radio-sign"></span>
                                                    Cash
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_options" value="Cheque">
                                                    <span class="form-radio-sign"></span>
                                                    Cheque
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_options" value="GCash">
                                                    <span class="form-radio-sign"></span>
                                                    GCash: &nbsp;&nbsp;
                                                    <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;" type="number" placeholder="Account Number">
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline" style="margin-left: 8rem;">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_options" value="UnionBank Direct Deposit">
                                                    <span class="form-radio-sign"></span>
                                                    UnionBank Direct Deposit: &nbsp;&nbsp;
                                                    <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;" type="number" placeholder="Account Number">
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline" style="margin-left: 8rem;">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_options" value="BPI Direct Deposit">
                                                    <span class="form-radio-sign"></span>
                                                    BPI Direct Deposit: &nbsp;&nbsp;
                                                    <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;" type="number" placeholder="Account Number">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <hr>
                                
                                <!-- =============== SPOUSE DETAILS =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Authority to Credit to My Bank Account</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group font-weight-normal">
                                            <p class="text-justify">I hereby authorize LIPIEMCO to directly credit to my <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width: 500px;"> account the withdrawn
                                            amount from my savings deposit. Such crediting to my bank/ATM/GCash account signifies my
                                            receipt of the said withdrawal transaction.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <!-- =============== CASHIER PROCESSING =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Cashier Processing</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-with-links">
                                    <div class="card-body table-full-width table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Process Step</th>
                                                <th>By</th>
                                                <th>Status</th>
                                                <th>Remarks</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="text" name="date_submitted" id="date-picker" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="last_name" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="first_name" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="loan_type" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td>
                                                        <select name="status" style="border:none;color:#888888" class="text-body">
                                                            <option value='0'>For Processing</option>
                                                            <option value='1'>GM Approval</option>
                                                            <option value='2'>On Hold</option>
                                                            <option value='3'>Cancelled</option>
                                                            <option value='4'>Denied</option>
                                                            <option value='5'>For Disbursement</option>
                                                            <option value='5'>Disbursed/Released</option>
                                                        </select>
                                                    </td>
                                                    <td><textarea type="text" rows="1" name="loan_amount" class="form-control bg-transparent border-white text-body" style="resize:none;"></textarea></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <hr>
                            </div>
                            <div class="card-footer ">
                                <button type="button" class="btn btn-fill btn-primary" disable>Submit</button>
                                <a href="{{ url('withdrawals/savingswithdrawal') }}" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            // date today
            var date = new Date().toISOString().slice(0,10);

            // Date picker set
            var loandatePicker = $("#loandate-picker");

            loandatePicker.datetimepicker({
                viewMode: "years",
                format: 'YYYY-MM-DD',
                minDate: moment().format("YYYY-MM-DD")
            });

            $("#loandate-picker").val(date);
            $("#date-picker").val(date);
            
        });

    </script>

@endsection