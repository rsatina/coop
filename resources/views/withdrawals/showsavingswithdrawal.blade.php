@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="">
                        @csrf
                        <div class="card">
                            @if ($errors)
                                <div class="card-header">
                                    <p class="error" style="color:red">{{ $errors->first() }}</p>
                                </div>
                            @endif
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-body">Date </label>
                                            <input type="text" placeholder="Current Date" name="withdrawal_date" class="form-control @error('withdrawal_date') is-invalid @enderror">
                                            @error('withdrawal_date')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Savings Withdrawal No. </label>
                                            <input type="text" placeholder="Loan Application No." name="savings_withdrawal_no" class="form-control @error('savings_withdrawal_no') is-invalid @enderror">
                                            @error('savings_withdrawal_no')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <!-- =============== Savings Withdrawal =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Savings Withdrawal</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="savings_type" class="text-body">Type of Savings <star class="star">*</star></label>
                                        <div class="row" id="savings_type">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="savings_type[]" value="Regular Savings">
                                                    <span class="form-radio-sign"></span>
                                                    Regular Savings
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="savings_type[]" value="Special Savings">
                                                    <span class="form-radio-sign"></span>
                                                    Special Savings
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="savings_type[]" value="Kiddie Savings">
                                                    <span class="form-radio-sign"></span>
                                                    Kiddie Savings
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('borrower_last_name', app('request')->input('borrower_last_name')) }}" name="borrower_last_name" class="form-control @error('borrower_last_name') is-invalid @enderror" required>
                                            @error('borrower_last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('borrower_first_name', app('request')->input('borrower_first_name')) }}" name="borrower_first_name" class="form-control @error('borrower_first_name') is-invalid @enderror" required>
                                            @error('borrower_first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('borrower_middle_name', app('request')->input('borrower_middle_name')) }}" name="borrower_middle_name" class="form-control @error('borrower_middle_name') is-invalid @enderror">
                                            @error('borrower_middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="borrower_email_address" class="form-control @error('borrower_email_address') is-invalid @enderror">
                                            @error('borrower_email_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="borrower_contact_no" class="form-control @error('borrower_contact_no') is-invalid @enderror">
                                            @error('borrower_contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">LIPIEMCO Account No. <star class="star">*</star></label>
                                            <input type="number" placeholder="LIPIEMCO Account No." name="lipiemco_account_no" class="form-control @error('lipiemco_account_no') is-invalid @enderror">
                                            @error('lipiemco_account_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Amount <star class="star">*</star></label>
                                            <input type="text" placeholder="Amount in words" name="amount_words" class="form-control @error('amount_words') is-invalid @enderror">
                                            @error('amount_words')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Php <star class="star">*</star></label>
                                            <input type="number" placeholder="Amount in Figure" name="amount_figure" class="form-control @error('amount_figure') is-invalid @enderror">
                                            @error('amount_figure')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="payment_options" class="text-body">Options <star class="star">*</star></label>
                                        <div class="row" id="payment_options">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_options[]" value="Cash">
                                                    <span class="form-radio-sign"></span>
                                                    Cash
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_options[]" value="Cheque">
                                                    <span class="form-radio-sign"></span>
                                                    Cheque
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_options[]" value="GCash">
                                                    <span class="form-radio-sign"></span>
                                                    GCash:
                                                    <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;" type="text">
                                                </label>
                                            </div>
                                        </div>
                                        <p></p>
                                        <div class="row" id="payment_options">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_options[]" value="UnionBank Direct Deposit">
                                                    <span class="form-radio-sign"></span>
                                                    UnionBank Direct Deposit:
                                                    <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;" type="text">
                                                </label>
                                            </div>
                                        </div>
                                        <p></p>
                                        <div class="row" id="payment_options">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_options[]" value="BPI Direct Deposit">
                                                    <span class="form-radio-sign"></span>
                                                    BPI Direct Deposit:
                                                    <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;" type="text">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <hr>

                                <p><strong>Kiddie Savings Details</strong></p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Child's Last Name <star class="star">*</star></label>
                                            <input type="email" placeholder="Name of Employer" name="borrower_employer_name" class="form-control @error('borrower_employer_name') is-invalid @enderror" disabled>
                                            @error('borrower_employer_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Child's First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" name="borrower_position" class="form-control @error('borrower_position') is-invalid @enderror" disabled>
                                            @error('borrower_position')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Child's Middle Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" name="borrower_position" class="form-control @error('borrower_position') is-invalid @enderror" disabled>
                                            @error('borrower_position')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <!-- =============== SPOUSE DETAILS =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Authority to Credit to My Bank Account</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group font-weight-normal">
                                            <p class="text-justify">I hereby authorize LIPIEMCO to directly credit to my <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width: 500px;"> account the withdrawn
                                            amount from my savings deposit. Such crediting to my bank/ATM/GCash account signifies my
                                            receipt of the said withdrawal transaction.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                            </div>
                            <div class="card-footer ">
                                <button type="button" class="btn btn-fill btn-primary" disable>Submit</button>
                                <a href="{{ url('withdrawals/savingswithdrawal') }}" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {

           
        });

    </script>

@endsection