@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="">
                        @csrf
                        <div class="card">
                            @if ($errors)
                                <div class="card-header">
                                    <p class="error" style="color:red">{{ $errors->first() }}</p>
                                </div>
                            @endif
                            
                            @if(auth()->user()->isAdministrator())
                                <div class="table-with-links">
                                    <div class="card-body table-full-width table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <th class="text-center">Withdrawal No.</th>
                                                <th>Date</th>
                                                <th>Last Name</th>
                                                <th>First Name</th>
                                                <th>Savings Type</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center">1</td>
                                                    <td><input type="text" name="date_submitted" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="last_name" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="first_name" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="loan_type" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="loan_amount" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td>
                                                        <select name="status" style="border:none;color:#888888" class="text-body">
                                                            <option value='0'>For Processing</option>
                                                            <option value='1'>GM Approval</option>
                                                            <option value='2'>On Hold</option>
                                                            <option value='3'>Cancelled</option>
                                                            <option value='4'>Denied</option>
                                                            <option value='5'>For Disbursement</option>
                                                            <option value='5'>Disbursed/Released</option>
                                                        </select>
                                                    </td>
                                                    <td class="td-actions">
                                                        <a href="{{ url('withdrawals/processsavingswithdrawal', ['id' => 1]) }}" rel="tooltip" title="Process Savings Withdrawal" class="btn btn-success btn-link btn-xs">
                                                            <i class="fa fa-hourglass-2"></i>
                                                        </a>
                                                        <a href="" rel="tooltip" title="For Approval" class="btn btn-success btn-link btn-xs">
                                                            <i class="fa fa-address-book-o"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @else
                                <div class="card-body ">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-body">Date </label>
                                                <div class='input-group date' id='loan-date'>
                                                    <input type='text' value="{{ date('M-d-Y', strtotime($withdrawal->created_at)) }}" class="form-control datepicker " disabled/>
                                                    <label class="input-group-append input-group-text" for="withdrawal_date" style="margin:inherit;border-radius:1px;">
                                                        <span class="fa fa-calendar"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body" for=>Savings Withdrawal No:</label>
                                                <input type="text" value="{{ $withdrawal->savings_withdrawal_no }}" style="border-style: none none none none;"  disabled>
                                                @error('loan_application_no')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <!-- =============== Savings Withdrawal =============== -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Savings Withdrawal</strong></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="savings_type" class="text-body">Type of Savings <star class="star">*</star></label>
                                            <div class="row" id="savings_type">
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body" style="margin-left: -5px;">
                                                        <input class="form-radio-input" id="rs" type="radio" disabled <?php echo (($withdrawal->savings_type == "Regular Savings") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Regular Savings
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" id="ss" type="radio" disabled <?php echo (($withdrawal->savings_type == "Special Savings") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Special Savings
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" id="ks" type="radio" disabled <?php echo (($withdrawal->savings_type == "Kiddie Savings") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Kiddie Savings
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </br>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Last Name <star class="star">*</star></label>
                                                <input type="text" value="{{ $withdrawal->lastname }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">First Name <star class="star">*</star></label>
                                                <input type="text" value="{{ $withdrawal->firstname }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Middle Name </label>
                                                <input type="text" value="{{ $withdrawal->middlename }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Email Address <star class="star">*</star></label>
                                                <input type="email" value="{{ $withdrawal->withdrawal_email }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Contact No. <star class="star">*</star></label>
                                                <input type="number" value="{{ $withdrawal->contact_no }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="text-body">LIPIEMCO Account No. <star class="star">*</star></label>
                                                <input type="number" value="{{ $withdrawal->lipiemco_acct_no}}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Amount <star class="star">*</star></label>
                                                <input type="text" value="{{ $withdrawal->amount_words }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Php <star class="star">*</star></label>
                                                <input type="number" value="{{ $withdrawal->amount_figure }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="mode_payment" class="text-body">Options <star class="star">*</star></label>
                                            <div class="form-check checkbox-inline ml-2">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="mode_payment" value="Cash" disabled <?php echo (($withdrawal->mode_payment == "Cash") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Cash
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline ml-5">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="mode_payment" value="Cheque" disabled <?php echo (($withdrawal->mode_payment == "Cheque") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Cheque
                                                </label>
                                            </div>
                                            <div class="form-check pl-5 ml-5">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="mode_payment" value="Unionbank Direct Deposit" disabled <?php echo (($withdrawal->mode_payment == "Unionbank Direct Deposit") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Unionbank Direct Deposit
                                                    <input class="form-check-input ml-2" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;width:150px" type="text">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-check checkbox-inline" style="margin-left: 8rem;">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="mode_payment" value="GCash" disabled <?php echo (($withdrawal->mode_payment == "GCash") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    GCash
                                                    <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;width:230px" type="text">
                                                </label>
                                            </div>
                                            <div class="form-check checkbox" style="margin-left: 8rem;">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="mode_payment" value="BPI Direct Deposit" disabled <?php echo (($withdrawal->mode_payment == "BPI Direct Deposit") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    BPI Direct Deposit
                                                    <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;width:150px" type="text">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <p id="kiddie_savings_label"><strong>Kiddie Savings Details</strong></p>
                                    <div class="row" id="kiddie_savings">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Child's Last Name <star class="star">*</star></label>
                                                <input type="email" placeholder="Name of Employer" name="borrower_employer_name" class="form-control @error('borrower_employer_name') is-invalid @enderror" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Child's First Name <star class="star">*</star></label>
                                                <input type="text" placeholder="Position" name="borrower_position" class="form-control @error('borrower_position') is-invalid @enderror" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Child's Middle Name <star class="star">*</star></label>
                                                <input type="text" placeholder="Position" name="borrower_position" class="form-control @error('borrower_position') is-invalid @enderror" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="form-control text-center text-light" style="background: gray;"><strong>Authority to Credit to My Bank Account</strong></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group font-weight-normal">
                                                <p class="text-justify">I hereby authorize LIPIEMCO to directly credit to my <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width: 500px;"> account the withdrawn
                                                amount from my savings deposit. Such crediting to my bank/ATM/GCash account signifies my
                                                receipt of the said withdrawal transaction.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                </div>
                                <div class="card-footer ">
                                    <a href="{{ url()->previous() }}" class="btn btn-fill btn-default">Back</a>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            // date today
            //var date = new Date().toISOString().slice(0,10);

            // Date picker set
            //var loandatePicker = $("#withdrawal_date");

            //loandatePicker.datetimepicker({
            //    viewMode: "years",
            //    format: 'YYYY-MM-DD',
            //    minDate: moment().format("YYYY-MM-DD")
            //});

            //$("#withdrawal_date").val(date);
            
            $('#kiddie_savings').hide();
            $('#kiddie_savings_label').hide();
            
            if ($('#ks').prop("checked") == true) {
                $('#kiddie_savings').show();
                $('#kiddie_savings_label').show();
            }
        });

        $('#rs').click(function(){
            $('#kiddie_savings').hide();
            $('#kiddie_savings_label').hide();
            $('#child_firstname').attr('disabled', 'disabled');
            $('#child_lastname').attr('disabled', 'disabled');
            $('#child_middlename').attr('disabled', 'disabled');
        })
        $('#ss').click(function(){
            $('#kiddie_savings').hide();
            $('#kiddie_savings_label').hide();
            $('#child_firstname').attr('disabled', 'disabled');
            $('#child_lastname').attr('disabled', 'disabled');
            $('#child_middlename').attr('disabled', 'disabled');
        })
        $('#ks').click(function(){
            $('#kiddie_savings').show();
            $('#kiddie_savings_label').show();
            $('#child_firstname').removeAttr('disabled');
            $('#child_lastname').removeAttr('disabled');
            $('#child_middlename').removeAttr('disabled');
        })
    </script>

@endsection