@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                <i class="nc-icon nc-simple-remove"></i>
                            </button>
                            <span>
                                <b>
                                    {{ session('message') }}
                                </b>
                            </span>
                        </div>
                    @endif
                    @if ($errors->any())
                        <ul>
                        @foreach ($errors->all() as $message)
                            <li> {{ $message }} </li>
                        @endforeach
                        </ul>
                    @endif
                    <form method="GET" action="">
                        @csrf
                        <input type="hidden" name="schedule_date">
                        <input type="hidden" name="schedule_time">
                        <input type="hidden" name="requirements" value="{{ app('request')->input('requirements') }}">
                        <input type="hidden" name="middle_name" value="{{ app('request')->input('middle_name') }}">
                        <input type="hidden" id="birthdate" name="birthdate" value="{{ app('request')->input('birthdate') }}">
                        <div class="card stacked-form">
                            <div class="card-header text-center" style="background-color:green">
                                <h4 class="card-title text-light pb-2" ><strong>Promissory Note<strong></h4>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-12 pt-3">
                                        <div class="form-group font-weight-normal">
                                            <p>PN Number : <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width:250px;"></p>
                                            <p>Date of Note: <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width:245px;"></p>
                                            <p>Maturity Date: <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width:240px;"></p>
                                            </br>
                                            <p class="text-justify">For VALUE RECEIVED, I/We promise to pay, jointly and severally to the order of LIPI EMPLOYEES MULTIPURPOSE COOPERATIVE (LIPIEMCO) the sum in PESOS:
                                               </br>*** <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width: 500px;">
                                               (Php <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;"> ),
                                               plus interest in the rate of <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;"> %
                                               per annum, payable in installments of <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;">
                                               (Php <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;"> )
                                               the first payment to be made on <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;">
                                               and a like amount every installment period until the full amount is fully paid.
                                            </p>
                                            
                                            </br>
                                            <p class="text-justify">The obligation contracted in this Note is resulting from a loan granted by LIPIEMCO to me/us. 
                                               This note is subject to a Real Estate or Chattel Mortgage, if applicable, to be executed by me/us 
                                               to secure this loan. Failure on my part to apply the said amount to the purpose for which it is 
                                               granted would render the whole amount of the loan due and demandable. In case of any failure or
                                               default in payments as herein agreed or failure on my/our part to make prompt payment on any
                                               of the monthly stipulated due dates shall render the entire amount of loan or the balance thereof
                                               due and demandable. Each party to this note whether as a maker, co-maker, endorser or
                                               guarantor; severally waive presentation of payment, demand protest and notice of protest of the
                                               same. Any partial payment or performance of this note or any extension thereof granted shall
                                               neither alter, novate nor vary the original terms of the obligation not discharge, and such partial
                                               payment of performance shall be considered as written acknowledgement of this obligation
                                               which interrupts the period of prescription.
                                            </p>
                                            </br>
                                            <p class="text-justify">To further secure this loan, I hereby pledge and assign to LIPIEMCO my salary and wage, 
                                               savings/time Deposits/Share Capital, patronage refunds and interest on Share Capital. In case of
                                               my separation or resignation from <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width: 500px;">
                                               employment before full payment of this loan, I likewise pledge and assign my retirement or
                                               separation pay, pension including but not limited to our advance pension, if any. For this
                                               purpose, I hereby appoint and designate with the understanding not to revoke this appointment/
                                               designation without written authority of LIPIEMCO, my employer or attorney-in-fact, to pay the
                                               loan by deducting or collecting from any or all of the aforementioned income due to me in such
                                               amount as to satisfy the principal, interest and penalties, if any, on said loan.
                                            </p>
                                            </br>
                                            <p class="text-justify">It is further agreed that in case payment shall not be made at maturity, I shall pay a penalty for
                                               2% per month plus collection cost, and attorney’s fees in addition to the principal and interest
                                               due on this note. Such charge in no event to be less than ONE THOUSAND PESOS (php
                                               1,000.00) and that said suit shall be subject to the jurisdiction of the courts of Cebu City, or 
                                               Mandaue City.
                                            </p>
                                            </br>
                                            <p class="text-justify">In witness whereof, I/We hereunto signed this instrument this <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;">
                                              day of <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;">
                                              at <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;">,
                                              Philippines.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer justify-content-right">
                                <button type="submit" name="action" class="btn btn-fill btn-primary" value="saveConsentForm">Save</button>
                                <a href="javascript:history.back()" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            var nowDate = new Date().getFullYear();
            var bDate = new Date($('#birthdate').val()).getFullYear();
            var a = nowDate - bDate;
            $('#age').val(a);
        });

    </script>
@endsection