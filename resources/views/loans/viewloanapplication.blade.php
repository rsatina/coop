@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="">
                        @csrf
                        <div class="card">
                            @if ($errors)
                                <div class="card-header">
                                    <p class="error" style="color:red">{{ $errors->first() }}</p>
                                </div>
                            @endif
                            @if(auth()->user()->isAdministrator())
                                <div class="table-with-links">
                                    <div class="card-body table-full-width table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <th class="text-center">Loan Application No.</th>
                                                <th>Date Submitted</th>
                                                <th>Last Name</th>
                                                <th>First Name</th>
                                                <th>Type of Loan</th>
                                                <th>Loan Amount</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center">1</td>
                                                    <td><input type="text" name="date_submitted" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="last_name" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="first_name" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="loan_type" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td><input type="text" name="loan_amount" class="form-control bg-transparent border-white text-body" disabled></input></td>
                                                    <td>
                                                        <select name="status" style="border:none;color:#888888" class="text-body">
                                                            <option value='0'>For Verification</option>
                                                            <option value='1'>Verified</option>
                                                            <option value='2'>For Evaluation</option>
                                                            <option value='3'>Loans Head Review</option>
                                                            <option value='4'>GM Approval</option>
                                                            <option value='5'>CreCom Approval</option>
                                                            <option value='5'>BOD Approval</option>
                                                            <option value='5'>Cashier Disbursement</option>
                                                            <option value='5'>Released/Deposited</option>
                                                            <option value='5'>Lacking Requirements</option>
                                                            <option value='5'>On-Hold</option>
                                                            <option value='5'>Cancelled</option>
                                                            <option value='5'>Denied</option>
                                                        </select>
                                                    </td>
                                                    <td class="td-actions">
                                                        <a href="{{ url('loans/showloanapplication', ['id' => 1]) }}" rel="tooltip" title="View Loan Application" class="btn btn-success btn-link btn-xs">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <a href="{{ url('loans/evaluateloanapplication', ['id' => 1]) }}" rel="tooltip" title="Evaluate Loan Application" class="btn btn-success btn-link btn-xs">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <a href="{{ url('loans/reviewapprovalloanapplication', ['id' => 1]) }}" rel="tooltip" title="Review/Approval Loan Application" class="btn btn-success btn-link btn-xs">
                                                            <i class="fa fa-newspaper-o"></i>
                                                        </a>
                                                        <a href="" rel="tooltip" title="Releasing Loan Application" class="btn btn-success btn-link btn-xs">
                                                            <i class="fa fa-address-book-o"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @else
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="loan_type" class="text-body">Type of Loan <star class="star">*</star></label>
                                            <div class="row" id="loan_type">
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="loan_type" disabled <?php echo (($loan->loan_type == "Providential Loan") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Providential Loan
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="loan_type" disabled <?php echo (($loan->loan_type == "Express Loan") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Express Loan
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="loan_type" disabled <?php echo (($loan->loan_type == "Instant Loan") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Instant Loan
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="loan_type" disabled <?php echo (($loan->loan_type == "Seasonal Loan") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Seasonal Loan
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="loan_type" disabled <?php echo (($loan->loan_type == "Business Loan") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Business Loan
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Date </label>
                                                <div class='input-group date' id='loan-date'>
                                                    <input type='text' id="loandate-picker" value="{{ date('M-d-Y', strtotime($loan->created_at)) }}" class="form-control datepicker" name="loan_date" placeholder="YYYY-MM-DD" disabled/>
                                                    <label class="input-group-append input-group-text" for="loandate-picker" style="margin:inherit;border-radius:1px;">
                                                        <span class="fa fa-calendar"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Loan Application No. </label>
                                                <input type="text" placeholder="Loan Application No." name="loan_application_no" style="border-style: none none none none;" value="{{ $loan->loan_application_no }}" disabled>
                                                @error('loan_application_no')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label class="text-body">Amount Applied <star class="star">*</star></label>
                                                <input type="text" placeholder="Amount Applied" value="{{ $loan->amount_word }}" name="amount_applied" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-body">Php <star class="star">*</star></label>
                                                <input type="number" placeholder="Amount" value="{{ $loan->amount_figure }}" name="amount" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label class="text-body">Purpose <star class="star">*</star></label>
                                                <input type="text" placeholder="Purpose" value="{{ $loan->purpose }}" name="purpose" class="form-control" disabled></input>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-body">Interest Rate <star class="star">*</star></label>
                                                <input type="number" placeholder="Interest Rate" value="{{ $loan->interest_rate }}" name="interest_rate" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-inline" style="font-size:12px"> TERMS APPLIED:
                                                <input type='text' class="ml-1" style="border-style: none none solid none;width:50px;font-size:15px" value="{{ $loan->term_applied }}" name="term_applied" disabled> months
                                            </div>
                                        </div>
                                        <div class="col-md-7" style="font-size:12px">
                                            <div class="row" id="payment_mode">
                                                MODE OF PAYMENT:
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="mode_payment" value="Post Dated Check" disabled <?php echo (($loan->mode_of_payment == "Post Dated Check") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Post Dated Check
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="mode_payment" value="Payroll Deduction" disabled <?php echo (($loan->mode_of_payment == "Payroll Deduction") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Payroll Deduction
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="mode_payment" value="Cash"disabled <?php echo (($loan->mode_of_payment == "Cash") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Cash
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p></p>

                                    <!-- =============== Member Borrower's Details =============== -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Member Borrower's Details</strong></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Last Name <star class="star">*</star></label>
                                                <input type="text" placeholder="Last Name" value="{{ $borrower->lastname }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">First Name <star class="star">*</star></label>
                                                <input type="text" value="{{ $borrower->firstname }}" class="form-control" disabled>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Middle Name </label>
                                                <input type="text" value="{{ $borrower->middlename }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label class="text-body">Present Address <star class="star">*</star></label>
                                                <input type="text" value="{{ $borrower->present_address }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="text-body">No. of Years of Stay <star class="star">*</star></label>
                                                <input type="number" value="{{ $borrower->no_yrs_stay }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <label for="payment_mode" class="text-body">Place of Residence <star class="star">*</star></label>
                                            <div class="row" id="mode_of_stay">
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body" style="margin-left: -5px;">
                                                        <input class="form-radio-input" type="radio" name="mode_of_stay" disabled <?php echo (($borrower->place_of_residence == "Owned") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Owned
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="mode_of_stay" value="Living with Parents/Relatives" disabled <?php echo (($borrower->place_of_residence == "Living with Parents/Relatives") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Living with Parents/Relatives
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="mode_of_stay" value="Renting" id="Renting" disabled <?php echo (($borrower->place_of_residence == "Renting") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Renting &nbsp;&nbsp;
                                                        <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;" type="text" value="{{ $borrower->property_owner }}" id="property_owner" disabled>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-check checkbox">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="lipiemco" disabled <?php echo (($borrower->lipiemco_staff == "LIPIEMCO Employee") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    LIPIEMCO Employee
                                                </label>
                                            </div>
                                            <div class="form-check checkbox">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="lipiemco" disabled <?php echo (($borrower->lipiemco_staff == "LIPIEMCO Officer") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    LIPIEMCO Officer
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="text-body">Provincial Address <star class="star">*</star></label>
                                                <input type="text" value="{{ $borrower->provincial_address }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Email Address <star class="star">*</star></label>
                                                <input type="email" value="{{ $borrower->borrower_email }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Contact No. <star class="star">*</star></label>
                                                <input type="number" value="{{ $borrower->contact_no }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Name of Employer <star class="star">*</star></label>
                                                <input type="email" value="{{ $borrower->employer_name }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Position <star class="star">*</star></label>
                                                <input type="text" value="{{ $borrower->position }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9"  style="font-size:12px">
                                            <div class="row ml-1">
                                                EMPLOYMENT STATUS:
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="employment_status" value="Probitionary" disabled <?php echo (($borrower->employer_status == "Probitionary") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Probitionary
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="employment_status" value="Regular" disabled <?php echo (($borrower->employer_status == "Regular") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Regular
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="employment_status" value="Contractual" disabled <?php echo (($borrower->employer_status == "Contractual") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Contractual
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="employment_status" value="Project-Based" disabled <?php echo (($borrower->employer_status == "Project-Based") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Project-Based
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-inline" style="font-size:12px"> YRS OF EMPLOYMENT:
                                                <input type='text' value="{{ $borrower->yrs_employment }}" class="ml-2" style="border-style: none none solid none;width:50px;font-size:15px">
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>
                                    <!-- =============== SPOUSE DETAILS =============== -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Spouse's Details</strong></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Last Name <star class="star">*</star></label>
                                                <input type="text" value="{{ $spouse->first()->lastname }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">First Name <star class="star">*</star></label>
                                                <input type="text" value="{{ $spouse->first()->firstname }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Middle Name </label>
                                                <input type="text" value="{{ $spouse->first()->middlename }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Email Address <star class="star">*</star></label>
                                                <input type="email" value="{{ $spouse->first()->spouse_email }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Contact No. <star class="star">*</star></label>
                                                <input type="number" value="{{ $spouse->first()->contact_no }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Name of Employer <star class="star">*</star></label>
                                                <input type="email" value="{{ $spouse->first()->employer_name }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Position <star class="star">*</star></label>
                                                <input type="text" value="{{ $spouse->first()->position }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-md-9"  style="font-size:12px">
                                            <div class="row ml-1">
                                                EMPLOYMENT STATUS:
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Probitionary" disabled <?php echo (($spouse->first()->employment_status == "Probitionary") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Probitionary
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Regular" disabled <?php echo (($spouse->first()->employment_status == "Regular") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Regular
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Contractual" disabled <?php echo (($spouse->first()->employment_status == "Contractual") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Contractual
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Project-Based" disabled <?php echo (($spouse->first()->employment_status == "Project-Based") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Project-Based
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-inline" style="font-size:12px"> YRS OF EMPLOYMENT:
                                                <input type='text' class="ml-2" style="border-style: none none solid none;width:50px;font-size:15px" value="{{ $spouse->first()->yrs_employment }}">
                                            </div>
                                        </div>
                                    </div>
                                    <p></p>
                                    <!-- =============== Co-Maker Details (1) =============== -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Co-Maker Details (1)</strong></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Last Name <star class="star">*</star></label>
                                                <input type="text" value="{{ $comaker->first()->lastname }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">First Name <star class="star">*</star></label>
                                                <input type="text" value="{{ $comaker->first()->firstname }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Middle Name </label>
                                                <input type="text" value="{{ $comaker->first()->middlename }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Email Address <star class="star">*</star></label>
                                                <input type="email" value="{{ $comaker->first()->comaker_email }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Contact No. <star class="star">*</star></label>
                                                <input type="number" value="{{ $comaker->first()->contact_no }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Name of Employer <star class="star">*</star></label>
                                                <input type="email" value="{{ $comaker->first()->employer_name }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Position <star class="star">*</star></label>
                                                <input type="text" value="{{ $comaker->first()->position }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-md-9"  style="font-size:12px">
                                            <div class="row ml-1">
                                                EMPLOYMENT STATUS:
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Probitionary" disabled <?php echo (($comaker->first()->employment_status == "Probitionary") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Probitionary
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Regular" disabled <?php echo (($comaker->first()->employment_status == "Regular") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Regular
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Contractual" disabled <?php echo (($comaker->first()->employment_status == "Contractual") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Contractual
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Project-Based" disabled <?php echo (($comaker->first()->employment_status == "Project-Based") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Project-Based
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-inline" style="font-size:12px"> YRS OF EMPLOYMENT:
                                                <input type='text' value="{{ $comaker->first()->yrs_employment }}" class="ml-2" style="border-style: none none solid none;width:50px;font-size:15px" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <p></p>

                                    <!-- =============== Co-Maker Details (2) =============== -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Co-Maker Details (2)</strong></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Last Name <star class="star">*</star></label>
                                                <input type="text" value="{{ $comaker->last()->lastname }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">First Name <star class="star">*</star></label>
                                                <input type="text" value="{{ $comaker->last()->firstname}}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Middle Name </label>
                                                <input type="text" value="{{ $comaker->last()->middlname }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Email Address <star class="star">*</star></label>
                                                <input type="email" value="{{ $comaker->last()->comaker_email }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Contact No. <star class="star">*</star></label>
                                                <input type="number" value="{{ $comaker->last()->contact_no }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Name of Employer <star class="star">*</star></label>
                                                <input type="email" value="{{ $comaker->last()->employer_name }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Position <star class="star">*</star></label>
                                                <input type="text" value="{{ $comaker->last()->position }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-md-9"  style="font-size:12px">
                                            <div class="row ml-1">
                                                EMPLOYMENT STATUS:
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Probitionary" disabled <?php echo (($comaker->last()->employment_status == "Probitionary") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Probitionary
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Regular" disabled <?php echo (($comaker->last()->employment_status == "Regular") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Regular
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Contractual" disabled <?php echo (($comaker->last()->employment_status == "Contractual") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Contractual
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Project-Based" disabled <?php echo (($comaker->last()->employment_status == "Project-Based") ? 'checked' : '');?>>
                                                        <span class="form-radio-sign"></span>
                                                        Project-Based
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-inline" style="font-size:12px"> YRS OF EMPLOYMENT:
                                                <input type='text' value="{{ $comaker->last()->yrs_employment }}" class="ml-2" style="border-style: none none solid none;width:50px;font-size:15px" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <h4 class="card-title">Attached Files</h4>
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-2"></div>
                                        <div class="col-md-6" >
                                            <div class="card w-75 h-100  border border-danger" style="width: 18rem; background-color: #ffffb3">
                                                <div class="text-center font-weight-bold mt-2"> Required Forms</div>
                                                <div class="text-center">
                                                    @if(session()->has('screeningFormData'))
                                                        <a class="btn" href="{{ url ('loans/personaldisclosureform') }}" data-form-screen style="border:none; background-color: #ffffb3; color:green">
                                                            <strong>Personal Data Disclosure Authorization Form</strong>        
                                                        </a><i class="fa fa-check-square-o"></i>
                                                    @else
                                                        <a class="btn" href="{{ url ('loans/personaldisclosureform') }}" data-form-screen style="border:none; background-color: #ffffb3; color:blue">
                                                            <strong>Personal Data Disclosure Authorization Form</strong>
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="text-center">
                                                    @if(session()->has('consentFormData'))
                                                        <a class="btn" href="{{ url('loans/pledgedepositform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                            <strong>Pledge of Deposit Form</strong>
                                                        </a><i class="fa fa-check-square-o"></i>
                                                    @else
                                                        <a class="btn" href="{{ url('loans/pledgedepositform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                            <strong>Pledge of Deposit Form</strong>
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="text-center">
                                                    @if(session()->has('consentFormData'))
                                                        <a class="btn" href="{{ url('loans/authoritydeductform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                            <strong>Authority to Deduct Form</strong>
                                                        </a><i class="fa fa-check-square-o"></i>
                                                    @else
                                                        <a class="btn" href="{{ url('loans/authoritydeductform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                            <strong>Authority to Deduct Form</strong>
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="text-center">
                                                    @if(session()->has('consentFormData'))
                                                        <a class="btn" href="{{ url('loans/promissorynoteform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                            <strong>Promissory Note Form</strong>
                                                        </a><i class="fa fa-check-square-o"></i>
                                                    @else
                                                        <a class="btn" href="{{ url('loans/promissorynoteform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                            <strong>Promissory Note Form</strong>
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="card-footer">
                                    <a href="{{ url()->previous() }}" class="btn btn-fill btn-default">Back</a>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            // date today
            //var date = new Date().toISOString().slice(0,10);

            // Date picker set
            //var loandatePicker = $("#loandate-picker");

            //loandatePicker.datetimepicker({
            //    viewMode: "years",
            //    format: 'YYYY-MM-DD',
            //    minDate: moment().format("YYYY-MM-DD")
            //});

            //$("#loandate-picker").val(date);

            
        });

    </script>

@endsection