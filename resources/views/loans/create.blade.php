@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                    <i class="nc-icon nc-simple-remove"></i>
                                </button>
                                <span>
                                    <b>
                                        {{ session('message') }}
                                    </b>
                                </span>
                            </div>
                        @endif
                        @if ($errors)
                            <div class="card-header">
                                <p class="error" style="color:red">{{ $errors->first() }}</p>
                            </div>
                        @endif
                        @if(auth()->user()->isAdministrator())
                            <div class="table-with-links">
                                <div class="card-body table-bordered table-striped table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr class="d-flex" style="background-color:#182370">
                                                <th class="text-center col-2 text-white"><strong>Loan Application Number</strong></th>
                                                <th class="text-center col-2 text-white"><strong>Date Submitted</strong></th>
                                                <th class="text-center col-2 text-white"><strong>Full Name</strong></th>
                                                <th class="text-center col-1 text-white"><strong>Type of Loan</strong></th>
                                                <th class="text-center col-1 text-white"><strong>Loan Amount</strong></th>
                                                <th class="text-center col-2 text-white"><strong>Status</strong></th>
                                                <th class="text-center col-2 text-white"><strong>Action</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($loans as $key => $loan)
                                            <tr class="d-flex">
                                                <td class="text-center col-2">{{ $loan->loan_application_no }}</td>
                                                <td class="text-center col-2">{{ date('M-d-Y h:i A', strtotime($loan->created_at)) }}</td>
                                                <td class="text-center col-2">{{ $loan->firstname }} {{ $loan->lastname }}</td>
                                                <td class="text-center col-1">{{ $loan->loan_type }}</td>
                                                <td class="text-center col-1">{{ $loan->amount_figure }}</td>
                                                <td class="text-center col-2">{{ $loan->status }} </td>
                                                <td class="col-2" style="font-size:13px">
                                                    <a href="{{ url('loans/showloanapplication', ['id' => $loan->id]) }}">- View </a><br>
                                                    <a href="{{ url('loans/evaluateloanapplication', ['id' => $loan->id]) }}">- Evaluate </a><br>
                                                    <a href="{{ url('loans/reviewapprovalloanapplication', ['id' => $loan->id]) }}">- Review/Approval </a><br>
                                                    <a href="{{ url('loans/releaseloanapplication', ['id' => $loan->id]) }}">- Releasing </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                            <form method="POST" action="{{ url('loans/store') }}" enctype="multipart/form-data">
                                @csrf
                                <input type=hidden name="service_charge" id="service_charge" step="0.01">
                                <input type=hidden name="special_savings" id="special_savings" step="0.01">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="loan_type" class="text-body">Type of Loan <star class="star">*</star></label>
                                            <div class="row" id="loan_type">
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body" style="margin-left: -5px;">
                                                        <input class="form-radio-input" type="radio" name="loan_type" id="pl" value="Providential Loan" required>
                                                        <span class="form-radio-sign"></span>
                                                        Providential Loan
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="loan_type" id="el" value="Express Loan">
                                                        <span class="form-radio-sign"></span>
                                                        Express Loan
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="loan_type" id="il" value="Instant Loan">
                                                        <span class="form-radio-sign"></span>
                                                        Instant Loan
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="loan_type" id="sl" value="Seasonal Loan">
                                                        <span class="form-radio-sign"></span>
                                                        Seasonal Loan
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="loan_type" id="bl" value="Business Loan">
                                                        <span class="form-radio-sign"></span>
                                                        Business Loan
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Date </label>
                                                <div class='input-group date' id='loan-date'>
                                                    <input type='text' id="loandate-picker" class="form-control datepicker @error('loandate-picker') is-invalid @enderror" name="loan_date" placeholder="YYYY-MM-DD" required>
                                                    <label class="input-group-append input-group-text" for="loandate-picker" style="margin:inherit;border-radius:1px;">
                                                        <span class="fa fa-calendar"></span>
                                                    </label>
                                                </div>
                                                @error('loan_date')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Loan Application No: </label>
                                                <input type="text" name="loan_application_no" style="border-style: none none none none;">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label class="text-body">Amount Applied <star class="star">*</star></label>
                                                <input type="text" placeholder="Amount Applied" name="amount_applied" id="words" class="form-control @error('amount_applied') is-invalid @enderror" required readonly>
                                                @error('amount_applied')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-body">Php <star class="star">*</star></label>
                                                <input type="number" placeholder="Amount" name="amount" id="number" class="form-control @error('amount') is-invalid @enderror" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" required>
                                                @error('amount')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label class="text-body">Purpose <star class="star">*</star></label>
                                                <input type="text" placeholder="Purpose" name="purpose" class="form-control @error('purpose') is-invalid @enderror" style="resize: none;" required></input>
                                                @error('purpose')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-body">Interest Rate (%)<star class="star">*</star></label>
                                                <input type="number" placeholder="Interest Rate" step="0.01" name="interest_rate" id="interest" class="form-control @error('interest_rate') is-invalid @enderror" required readonly>
                                                @error('interest_rate')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-inline" style="font-size:12px"> TERMS APPLIED:
                                                <input type='text' class="ml-1" style="border-style: none none solid none;width:50px;font-size:15px" name="term_applied" required> months
                                            </div>
                                        </div>
                                        <div class="col-md-7" style="font-size:12px">
                                            <div class="row" id="payment_mode">
                                                MODE OF PAYMENT:
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="mode_payment" value="Post Dated Check" required>
                                                        <span class="form-radio-sign"></span>
                                                        Post Dated Check
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="mode_payment" value="Payroll Deduction">
                                                        <span class="form-radio-sign"></span>
                                                        Payroll Deduction
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="mode_payment" value="Cash">
                                                        <span class="form-radio-sign"></span>
                                                        Cash
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p></p>
                                    <!-- =============== Member Borrower's Details =============== -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Member Borrower's Details</strong></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Last Name <star class="star">*</star></label>
                                                <input type="text" placeholder="Last Name" value="{{ old('borrower_last_name', app('request')->input('borrower_last_name')) }}" name="borrower_last_name" class="form-control @error('borrower_last_name') is-invalid @enderror" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">First Name <star class="star">*</star></label>
                                                <input type="text" placeholder="First Name" value="{{ old('borrower_first_name', app('request')->input('borrower_first_name')) }}" name="borrower_first_name" class="form-control @error('borrower_first_name') is-invalid @enderror" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Middle Name </label>
                                                <input type="text" placeholder="Middle Name" value="{{ old('borrower_middle_name', app('request')->input('borrower_middle_name')) }}" name="borrower_middle_name" class="form-control @error('borrower_middle_name') is-invalid @enderror" required>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label class="text-body">Present Address <star class="star">*</star></label>
                                                <input type="text" placeholder="Present Address" value="{{ old('borrower_present_address', app('request')->input('borrower_present_address')) }}" name="borrower_present_address" class="form-control @error('borrower_present_address') is-invalid @enderror" required>
                                                @error('borrower_present_address')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="text-body">No. of Years of Stay <star class="star">*</star></label>
                                                <input type="number" placeholder="No. of Years of Stay" name="borrower_years_of_stay" class="form-control @error('borrower_years_of_stay') is-invalid @enderror" required>
                                                @error('borrower_years_of_stay')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <label for="payment_mode" class="text-body">Place of Residence <star class="star">*</star></label>
                                            <div class="row" id="mode_of_stay">
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body" style="margin-left: -5px;">
                                                        <input class="form-radio-input" type="radio" name="mode_of_stay" value="Owned" onclick="HidePropertyOwner()" required>
                                                        <span class="form-radio-sign"></span>
                                                        Owned
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="mode_of_stay" value="Living with Parents/Relatives" onclick="HidePropertyOwner()">
                                                        <span class="form-radio-sign"></span>
                                                        Living with Parents/Relatives
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="mode_of_stay" value="Renting" id="Renting" onclick="ShowPropertyOwner()">
                                                        <span class="form-radio-sign"></span>
                                                        Renting &nbsp;&nbsp;
                                                        <input class="form-check-input" style="margin-left: 0px; margin-top: 0px; border-left: none; border-right: none; border-top: none;" type="text" name="property_owner" id="property_owner" placeholder="property owner" disabled="disabled">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-check checkbox">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="lipiemco" value="LIPIEMCO Employee">
                                                    <span class="form-radio-sign"></span>
                                                    LIPIEMCO Employee
                                                </label>
                                            </div>
                                            <div class="form-check checkbox">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="lipiemco" value="LIPIEMCO Officer">
                                                    <span class="form-radio-sign"></span>
                                                    LIPIEMCO Officer
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="text-body">Provincial Address <star class="star">*</star></label>
                                                <input type="text" placeholder="Provincial Address" name="provincial_address" class="form-control @error('provincial_address') is-invalid @enderror" required>
                                                @error('provincial_address')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Email Address <star class="star">*</star></label>
                                                <input type="email" placeholder="Email Address" name="borrower_email" class="form-control @error('borrower_email') is-invalid @enderror" required>
                                                @error('borrower_email')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Contact No. <star class="star">*</star></label>
                                                <input type="number" placeholder="Contact No." name="borrower_contact_no" class="form-control @error('borrower_contact_no') is-invalid @enderror" required>
                                                @error('borrower_contact_no')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Name of Employer <star class="star">*</star></label>
                                                <input type="text" placeholder="Name of Employer" name="borrower_employer_name" class="form-control @error('borrower_employer_name') is-invalid @enderror" required>
                                                @error('borrower_employer_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Position <star class="star">*</star></label>
                                                <input type="text" placeholder="Position" name="borrower_position" class="form-control @error('borrower_position') is-invalid @enderror" required>
                                                @error('borrower_position')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-md-9"  style="font-size:12px">
                                            <div class="row ml-1">
                                                EMPLOYMENT STATUS:
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="employment_status" value="Probitionary" required>
                                                        <span class="form-radio-sign"></span>
                                                        Probitionary
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="employment_status" value="Regular">
                                                        <span class="form-radio-sign"></span>
                                                        Regular
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="employment_status" value="Contractual">
                                                        <span class="form-radio-sign"></span>
                                                        Contractual
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="employment_status" value="Project-Based">
                                                        <span class="form-radio-sign"></span>
                                                        Project-Based
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-inline" style="font-size:12px"> YRS OF EMPLOYMENT:
                                                <input type='text' class="ml-2" style="border-style: none none solid none;width:50px;font-size:15px" name="borrower_years_of_employment" required>
                                            </div>
                                        </div>
                                    </div>
                                    </br>

                                    <!-- =============== SPOUSE DETAILS =============== -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Spouse's Details</strong></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Last Name <star class="star">*</star></label>
                                                <input type="text" placeholder="Last Name" value="{{ old('spouse_last_name', app('request')->input('spouse_last_name')) }}" name="spouse_last_name" class="form-control @error('spouse_last_name') is-invalid @enderror">
                                                @error('spouse_last_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">First Name <star class="star">*</star></label>
                                                <input type="text" placeholder="First Name" value="{{ old('spouse_first_name', app('request')->input('spouse_first_name')) }}" name="spouse_first_name" class="form-control @error('spouse_first_name') is-invalid @enderror">
                                                @error('spouse_first_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Middle Name </label>
                                                <input type="text" placeholder="Middle Name" value="{{ old('spouse_middle_name', app('request')->input('spouse_middle_name')) }}" name="spouse_middle_name" class="form-control @error('spouse_middle_name') is-invalid @enderror">
                                                @error('spouse_middle_name')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Email Address <star class="star">*</star></label>
                                                <input type="email" placeholder="Email Address" name="spouse_email" class="form-control @error('spouse_email') is-invalid @enderror">
                                                @error('spouse_email')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Contact No. <star class="star">*</star></label>
                                                <input type="number" placeholder="Contact No." name="spouse_contact_no" class="form-control @error('spouse_contact_no') is-invalid @enderror">
                                                @error('spouse_contact_no')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Name of Employer <star class="star">*</star></label>
                                                <input type="text" placeholder="Name of Employer" name="spouse_employer_name" class="form-control @error('spouse_employer_name') is-invalid @enderror">
                                                @error('spouse_employer_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Position <star class="star">*</star></label>
                                                <input type="text" placeholder="Position" name="spouse_position" class="form-control @error('spouse_position') is-invalid @enderror">
                                                @error('spouse_position')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9"  style="font-size:12px">
                                            <div class="row ml-1">
                                                EMPLOYMENT STATUS:
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Probitionary">
                                                        <span class="form-radio-sign"></span>
                                                        Probitionary
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Regular">
                                                        <span class="form-radio-sign"></span>
                                                        Regular
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Contractual">
                                                        <span class="form-radio-sign"></span>
                                                        Contractual
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Project-Based">
                                                        <span class="form-radio-sign"></span>
                                                        Project-Based
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-inline" style="font-size:12px"> YRS OF EMPLOYMENT:
                                                <input type='text' class="ml-2" style="border-style: none none solid none;width:50px;font-size:15px" name="spouse_years_of_employment"/>
                                            </div>
                                        </div>
                                    </div>
                                    <p></p>

                                    <!-- =============== Co-Maker Details (1) =============== -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Co-Maker Details (1)</strong></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Last Name <star class="star">*</star></label>
                                                <input type="text" placeholder="Last Name" value="{{ old('coMaker1_last_name', app('request')->input('coMaker1_last_name')) }}" name="coMaker1_last_name" class="form-control @error('coMaker1_last_name') is-invalid @enderror" required>
                                                @error('coMaker1_last_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">First Name <star class="star">*</star></label>
                                                <input type="text" placeholder="First Name" value="{{ old('coMaker1_first_name', app('request')->input('coMaker1_first_name')) }}" name="coMaker1_first_name" class="form-control @error('coMaker1_first_name') is-invalid @enderror" required>
                                                @error('coMaker1_first_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Middle Name </label>
                                                <input type="text" placeholder="Middle Name" value="{{ old('coMaker1_middle_name', app('request')->input('coMaker1_middle_name')) }}" name="coMaker1_middle_name" class="form-control @error('coMaker1_middle_name') is-invalid @enderror" required>
                                                @error('coMaker1_middle_name')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Email Address <star class="star">*</star></label>
                                                <input type="email" placeholder="Email Address" name="coMaker1_email" class="form-control @error('coMaker1_email') is-invalid @enderror" required>
                                                @error('coMaker1_email')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Contact No. <star class="star">*</star></label>
                                                <input type="number" placeholder="Contact No." name="coMaker1_contact_no" class="form-control @error('coMaker1_contact_no') is-invalid @enderror" required>
                                                @error('coMaker1_contact_no')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Name of Employer <star class="star">*</star></label>
                                                <input type="text" placeholder="Name of Employer" name="coMaker1_employer_name" class="form-control @error('coMaker1_employer_name') is-invalid @enderror" required>
                                                @error('coMaker1_employer_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Position <star class="star">*</star></label>
                                                <input type="text" placeholder="Position" name="coMaker1_position" class="form-control @error('coMaker1_position') is-invalid @enderror" required>
                                                @error('coMaker1_position')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-md-9"  style="font-size:12px">
                                            <div class="row ml-1">
                                                EMPLOYMENT STATUS:
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Probitionary" required>
                                                        <span class="form-radio-sign"></span>
                                                        Probitionary
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Regular">
                                                        <span class="form-radio-sign"></span>
                                                        Regular
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Contractual">
                                                        <span class="form-radio-sign"></span>
                                                        Contractual
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Project-Based">
                                                        <span class="form-radio-sign"></span>
                                                        Project-Based
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-inline" style="font-size:12px"> YRS OF EMPLOYMENT:
                                                <input type='text' class="ml-2" style="border-style: none none solid none;width:50px;font-size:15px" name="coMaker1_years_of_employment" required>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- =============== Co-Maker Details (2) =============== -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Co-Maker Details (2)</strong></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Last Name <star class="star">*</star></label>
                                                <input type="text" placeholder="Last Name" value="{{ old('coMaker2_last_name', app('request')->input('coMaker2_last_name')) }}" name="coMaker2_last_name" class="form-control @error('coMaker2_last_name') is-invalid @enderror" required>
                                                @error('coMaker2_last_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">First Name <star class="star">*</star></label>
                                                <input type="text" placeholder="First Name" value="{{ old('coMaker2_first_name', app('request')->input('coMaker2_first_name')) }}" name="coMaker2_first_name" class="form-control @error('coMaker2_first_name') is-invalid @enderror" required>
                                                @error('coMaker2_first_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Middle Name </label>
                                                <input type="text" placeholder="Middle Name" value="{{ old('coMaker2_middle_name', app('request')->input('coMaker2_middle_name')) }}" name="coMaker2_middle_name" class="form-control @error('coMaker2_middle_name') is-invalid @enderror" required>
                                                @error('coMaker2_middle_name')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Email Address <star class="star">*</star></label>
                                                <input type="email" placeholder="Email Address" name="coMaker2_email" class="form-control @error('coMaker2_email') is-invalid @enderror" required>
                                                @error('coMaker2_email')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Contact No. <star class="star">*</star></label>
                                                <input type="number" placeholder="Contact No." name="coMaker2_contact_no" class="form-control @error('coMaker2_contact_no') is-invalid @enderror" required>
                                                @error('coMaker2_contact_no')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="text-body">Name of Employer <star class="star">*</star></label>
                                                <input type="text" placeholder="Name of Employer" name="coMaker2_employer_name" class="form-control @error('coMaker2_employer_name') is-invalid @enderror" required>
                                                @error('coMaker2_employer_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-body">Position <star class="star">*</star></label>
                                                <input type="text" placeholder="Position" name="coMaker2_position" class="form-control @error('coMaker2_position') is-invalid @enderror" required>
                                                @error('coMaker2_position')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-md-9"  style="font-size:12px">
                                            <div class="row ml-1">
                                                EMPLOYMENT STATUS:
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Probitionary" required>
                                                        <span class="form-radio-sign"></span>
                                                        Probitionary
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Regular">
                                                        <span class="form-radio-sign"></span>
                                                        Regular
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Contractual">
                                                        <span class="form-radio-sign"></span>
                                                        Contractual
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="text-body">
                                                        <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Project-Based">
                                                        <span class="form-radio-sign"></span>
                                                        Project-Based
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-inline" style="font-size:12px"> YRS OF EMPLOYMENT:
                                                <input type='text' class="ml-2" style="border-style: none none solid none;width:50px;font-size:15px" name="coMaker2_years_of_employment" required>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4 class="card-title">Attached Files</h4>
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-2"></div>
                                        <div class="col-md-6" >
                                            <div class="card w-75 h-100  border border-danger" style="width: 18rem; background-color: #ffffb3">
                                                <div class="text-center font-weight-bold mt-2"> Forms</div>
                                                <div class="text-center">
                                                    @if(session()->has('screeningFormData'))
                                                        <a class="btn" href="{{ url ('loans/personaldisclosureform') }}" data-form-screen style="border:none; background-color: #ffffb3; color:green">
                                                            <strong>Personal Data Disclosure Authorization Form</strong>        
                                                        </a><i class="fa fa-check-square-o"></i>
                                                    @else
                                                        <a class="btn" href="{{ url ('loans/personaldisclosureform') }}" data-form-screen style="border:none; background-color: #ffffb3; color:blue">
                                                            <strong>Personal Data Disclosure Authorization Form</strong>
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="text-center">
                                                    @if(session()->has('consentFormData'))
                                                        <a class="btn" href="{{ url('loans/pledgedepositform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                            <strong>Pledge of Deposit Form</strong>
                                                        </a><i class="fa fa-check-square-o"></i>
                                                    @else
                                                        <a class="btn" href="{{ url('loans/pledgedepositform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                            <strong>Pledge of Deposit Form</strong>
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="text-center">
                                                    @if(session()->has('consentFormData'))
                                                        <a class="btn" href="{{ url('loans/authoritydeductform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                            <strong>Authority to Deduct Form</strong>
                                                        </a><i class="fa fa-check-square-o"></i>
                                                    @else
                                                        <a class="btn" href="{{ url('loans/authoritydeductform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                            <strong>Authority to Deduct Form</strong>
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="text-center">
                                                    @if(session()->has('consentFormData'))
                                                        <a class="btn" href="{{ url('loans/promissorynoteform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                            <strong>Promissory Note Form</strong>
                                                        </a><i class="fa fa-check-square-o"></i>
                                                    @else
                                                        <a class="btn" href="{{ url('loans/promissorynoteform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                            <strong>Promissory Note Form</strong>
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="card-footer">
                                    <button type="submit" name="action" id="create_loan" value="create_loan" class="btn btn-fill btn-primary">Submit</button>
                                    <a href="{{ url('loans/create') }}" class="btn btn-fill btn-danger">Cancel</a>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">

    document.addEventListener('DOMContentLoaded', function() {
        // date today
        var date = new Date().toISOString().slice(0,10);

        // Date picker set
        var loandatePicker = $("#loandate-picker");

        loandatePicker.datetimepicker({
            viewMode: "years",
            format: 'YYYY-MM-DD',
            minDate: moment().format("YYYY-MM-DD")
        });

        $("#loandate-picker").val(date);

    });

    const arr = x => Array.from(x);
    const num = x => Number(x) || 0;
    const str = x => String(x);
    const isEmpty = xs => xs.length === 0;
    const take = n => xs => xs.slice(0,n);
    const drop = n => xs => xs.slice(n);
    const reverse = xs => xs.slice(0).reverse();
    const comp = f => g => x => f (g (x));
    const not = x => !x;
    const chunk = n => xs =>
    isEmpty(xs) ? [] : [take(n)(xs), ...chunk (n) (drop (n) (xs))];

    // numToWords :: (Number a, String a) => a -> String
    let numToWords = n => {
    
        let a = [
            '', 'One', 'Two', 'Three', 'Four',
            'Five', 'Six', 'Seven', 'Eight', 'Nine',
            'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen',
            'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'
        ];
        
        let b = [
            '', '', 'Twenty', 'Thirty', 'Forty',
            'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'
        ];
        
        let g = [
            '', 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion',
            'Quintillion', 'Sixtillion', 'Septillion', 'Octillion', 'Nonillion'
        ];
        
        // this part is really nasty still
        // it might edit this again later to show how Monoids could fix this up
        let makeGroup = ([ones,tens,huns]) => {
            return [
            num(huns) === 0 ? '' : a[huns] + ' Hundred ',
            num(ones) === 0 ? b[tens] : b[tens] && b[tens] + '-' || '',
            a[tens+ones] || a[ones]
            ].join('');
        };
        
        let thousand = (group,i) => group === '' ? group : `${group} ${g[i]}`;
        
        if (typeof n === 'number')
            return numToWords(String(n));
        else if (n === '0')
            return 'Zero';
        else
            return comp (chunk(3)) (reverse) (arr(n))
            .map(makeGroup)
            .map(thousand)
            .filter(comp(not)(isEmpty))
            .reverse()
            .join(' ');
    };

    document.getElementById('number').onkeyup = function () {
        $words_value = numToWords(document.getElementById('number').value);

        $('#words').val($words_value);
    };

    function ShowPropertyOwner() {
        var property_owner = document.getElementById("property_owner");
        property_owner.removeAttribute("disabled");
        property_owner.setAttribute("required", true);
    }

    function HidePropertyOwner() {
        var property_owner = document.getElementById("property_owner");
        property_owner.setAttribute("disabled", false);
    }

    $('#pl').click(function(){
        $('#interest').val('15.00');
        $('#service_charge').val('0');
        $('#special_savings').val('2.00');
    })
    $('#el').click(function(){
        $('#interest').val('12.00');
        $('#service_charge').val('50');
        $('#special_savings').val('0');
    })
    $('#il').click(function(){
        $('#interest').val('12.00');
        $('#service_charge').val('0');
        $('#special_savings').val('2.00');
    })
    $('#sl').click(function(){
        $('#interest').val('15.00');
        $('#service_charge').val('0');
        $('#special_savings').val('0');
    })
    $('#bl').click(function(){
        $('#interest').val('15.00');
        $('#service_charge').val('2.00');
        $('#special_savings').val('2.00');
    })

</script>

@endsection