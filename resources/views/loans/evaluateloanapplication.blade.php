@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="">
                        @csrf
                        <div class="card">
                            @if ($errors)
                                <div class="card-header">
                                    <p class="error" style="color:red">{{ $errors->first() }}</p>
                                </div>
                            @endif
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="loan_type" class="text-body">Type of Loan <star class="star">*</star></label>
                                        <div class="row" id="loan_type">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="loan_type" value="Providential Loan">
                                                    <span class="form-radio-sign"></span>
                                                    Providential Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="loan_type" value="Express Loan">
                                                    <span class="form-radio-sign"></span>
                                                    Express Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="loan_type" value="Instant Loan">
                                                    <span class="form-radio-sign"></span>
                                                    Instant Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="loan_type" value="Seasonal Loan">
                                                    <span class="form-radio-sign"></span>
                                                    Seasonal Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="loan_type" value="Business Loan">
                                                    <span class="form-radio-sign"></span>
                                                    Business Loan
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Loan Application No. </label>
                                            <input type="text" placeholder="Loan Application No." name="loan_application_no" class="form-control @error('loan_application_no') is-invalid @enderror" disabled>
                                            @error('loan_application_no')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Date </label>
                                            <input type="text" placeholder="Date" name="loan_date" class="form-control @error('loan_date') is-invalid @enderror" disabled>
                                            @error('loan_date')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Php <star class="star">*</star></label>
                                            <input type="number" placeholder="Amount" name="amount" class="form-control @error('amount') is-invalid @enderror" disabled>
                                            @error('amount')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Amount Applied <star class="star">*</star></label>
                                            <input type="text" placeholder="Amount Applied" name="amount_applied" class="form-control @error('amount_applied') is-invalid @enderror" disabled>
                                            @error('amount_applied')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">Purpose <star class="star">*</star></label>
                                            <textarea type="text" placeholder="Purpose" name="purpose" class="form-control @error('purpose') is-invalid @enderror" style="resize: none;" disabled></textarea>
                                            @error('purpose')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Term Applied (months) <star class="star">*</star></label>
                                            <input type="number" placeholder="Term Applied" name="term_applied" class="form-control @error('term_applied') is-invalid @enderror" disabled>
                                            @error('term_applied')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Interest Rate <star class="star">*</star></label>
                                            <input type="number" placeholder="Interest Rate" name="interest_rate" class="form-control @error('interest_rate') is-invalid @enderror" disabled>
                                            @error('interest_rate')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="payment_mode" class="text-body">Mode of Payment <star class="star">*</star></label>
                                        <div class="row" id="payment_mode">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="payment_mode_post_dated" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Post Dated Check
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_mode_payroll_deduction" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Payroll Deduction
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="payment_mode_cash" value="Contractual">
                                                    <span class="form-radio-sign"></span>
                                                    Cash
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <!-- =============== Member Borrower's Details =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Member Borrower's Details</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="row" id="lipiemco_staff">
                                        <div class="form-check checkbox-inline">
                                            <label class="text-body" style="margin-left: 10px;">
                                                <input class="form-radio-input" type="radio" name="lipiemco_staff[]" value="LIPIEMCO Employee">
                                                <span class="form-radio-sign"></span>
                                                LIPIEMCO Employee
                                            </label>
                                        </div>
                                        <div class="form-check checkbox-inline">
                                            <label class="text-body">
                                                <input class="form-radio-input" type="radio" name="lipiemco_staff[]" value="LIPIEMCO Officer">
                                                <span class="form-radio-sign"></span>
                                                LIPIEMCO Officer
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <p></p>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('borrower_last_name', app('request')->input('borrower_last_name')) }}" name="borrower_last_name" class="form-control @error('borrower_last_name') is-invalid @enderror" disabled>
                                            @error('borrower_last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('borrower_first_name', app('request')->input('borrower_first_name')) }}" name="borrower_first_name" class="form-control @error('borrower_first_name') is-invalid @enderror" disabled>
                                            @error('borrower_first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('borrower_middle_name', app('request')->input('borrower_middle_name')) }}" name="borrower_middle_name" class="form-control @error('borrower_middle_name') is-invalid @enderror" disabled>
                                            @error('borrower_middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Present Address <star class="star">*</star></label>
                                            <input type="text" placeholder="Present Address" value="{{ old('borrower_present_address', app('request')->input('borrower_present_address')) }}" name="borrower_present_address" class="form-control @error('borrower_present_address') is-invalid @enderror"  disabled>
                                            @error('borrower_present_address')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">No. of Years of Stay <star class="star">*</star></label>
                                            <input type="number" placeholder="No. of Years of Stay" name="borrower_years_of_stay" class="form-control @error('borrower_years_of_stay') is-invalid @enderror"  disabled>
                                            @error('borrower_years_of_stay')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="mode_of_stay" class="text-body">Place of Residence <star class="star">*</star></label>
                                        <div class="row" id="mode_of_stay">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="mode_of_stay" value="Owned">
                                                    <span class="form-radio-sign"></span>
                                                    Owned
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="mode_of_stay" value="Living with Parents/Relatives">
                                                    <span class="form-radio-sign"></span>
                                                    Living with Parents/Relatives
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="mode_of_stay" value="Renting">
                                                    <span class="form-radio-sign"></span>
                                                    Renting
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">Provincial Address <star class="star">*</star></label>
                                            <input type="text" placeholder="Provincial Address" name="provincial_address" class="form-control @error('provincial_address') is-invalid @enderror" disabled>
                                            @error('provincial_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="borrower_email_address" class="form-control @error('borrower_email_address') is-invalid @enderror" disabled>
                                            @error('borrower_email_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="borrower_contact_no" class="form-control @error('borrower_contact_no') is-invalid @enderror" disabled>
                                            @error('borrower_contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Name of Employer <star class="star">*</star></label>
                                            <input type="email" placeholder="Name of Employer" name="borrower_employer_name" class="form-control @error('borrower_employer_name') is-invalid @enderror" disabled>
                                            @error('borrower_employer_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Position <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" name="borrower_position" class="form-control @error('borrower_position') is-invalid @enderror" disabled>
                                            @error('borrower_position')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="employment_status" class="text-body">Employment Status <star class="star">*</star></label>
                                        <div class="row" id="employment_status">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="employment_status" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Probitionary
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="employment_status" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Regular
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="employment_status" value="Contractual">
                                                    <span class="form-radio-sign"></span>
                                                    Contractual
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="employment_status" value="Project-Based">
                                                    <span class="form-radio-sign"></span>
                                                    Project-Based
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Years of Employment <star class="star">*</star></label>
                                            <input type="number" placeholder="Years of Employment" name="borrower_years_of_employment" class="form-control @error('borrower_years_of_employment') is-invalid @enderror" disabled>
                                            @error('borrower_years_of_employment')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <!-- =============== SPOUSE DETAILS =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Spouse's Details</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('spouse_last_name', app('request')->input('spouse_last_name')) }}" name="spouse_last_name" class="form-control @error('spouse_last_name') is-invalid @enderror" disabled>
                                            @error('spouse_last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('spouse_first_name', app('request')->input('spouse_first_name')) }}" name="spouse_first_name" class="form-control @error('spouse_first_name') is-invalid @enderror" disabled>
                                            @error('spouse_first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('spouse_middle_name', app('request')->input('spouse_middle_name')) }}" name="spouse_middle_name" class="form-control @error('spouse_middle_name') is-invalid @enderror" disabled>
                                            @error('spouse_middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="spouse_email_address" class="form-control @error('spouse_email_address') is-invalid @enderror" disabled>
                                            @error('spouse_email_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="spouse_contact_no" class="form-control @error('spouse_contact_no') is-invalid @enderror" disabled>
                                            @error('spouse_contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Name of Employer <star class="star">*</star></label>
                                            <input type="email" placeholder="Name of Employer" name="spouse_employer_name" class="form-control @error('spouse_employer_name') is-invalid @enderror" disabled>
                                            @error('spouse_employer_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Position <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" name="spouse_position" class="form-control @error('spouse_position') is-invalid @enderror" disabled>
                                            @error('spouse_position')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="spouse_employment_status" class="text-body">Employment Status <star class="star">*</star></label>
                                        <div class="row" id="spouse_employment_status">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Probitionary
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Regular
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Contractual">
                                                    <span class="form-radio-sign"></span>
                                                    Contractual
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="spouse_employment_status" value="Project-Based">
                                                    <span class="form-radio-sign"></span>
                                                    Project-Based
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Years of Employment <star class="star">*</star></label>
                                            <input type="number" placeholder="Years of Employment" name="spouse_years_of_employment" class="form-control @error('spouse_years_of_employment') is-invalid @enderror" disabled>
                                            @error('spouse_years_of_employment')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <!-- =============== Co-Maker Details (1) =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Co-Maker Details (1)</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('coMaker1_last_name', app('request')->input('coMaker1_last_name')) }}" name="coMaker1_last_name" class="form-control @error('coMaker1_last_name') is-invalid @enderror" disabled>
                                            @error('coMaker1_last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('coMaker1_first_name', app('request')->input('coMaker1_first_name')) }}" name="coMaker1_first_name" class="form-control @error('coMaker1_first_name') is-invalid @enderror" disabled>
                                            @error('coMaker1_first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('coMaker1_middle_name', app('request')->input('coMaker1_middle_name')) }}" name="coMaker1_middle_name" class="form-control @error('coMaker1_middle_name') is-invalid @enderror" disabled>
                                            @error('coMaker1_middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="coMaker1_email_address" class="form-control @error('coMaker1_email_address') is-invalid @enderror" disabled>
                                            @error('coMaker1_email_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="coMaker1_contact_no" class="form-control @error('coMaker1_contact_no') is-invalid @enderror" disabled>
                                            @error('coMaker1_contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Name of Employer <star class="star">*</star></label>
                                            <input type="email" placeholder="Name of Employer" name="coMaker1_employer_name" class="form-control @error('coMaker1_employer_name') is-invalid @enderror" disabled>
                                            @error('coMaker1_employer_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Position <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" name="coMaker1_position" class="form-control @error('coMaker1_position') is-invalid @enderror" disabled>
                                            @error('coMaker1_position')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="coMaker1_employment_status" class="text-body">Employment Status <star class="star">*</star></label>
                                        <div class="row" id="coMaker1_employment_status">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Probitionary
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Regular
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Contractual">
                                                    <span class="form-radio-sign"></span>
                                                    Contractual
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker1_employment_status" value="Project-Based">
                                                    <span class="form-radio-sign"></span>
                                                    Project-Based
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Years of Employment <star class="star">*</star></label>
                                            <input type="number" placeholder="Years of Employment" name="coMaker1_years_of_employment" class="form-control @error('coMaker1_years_of_employment') is-invalid @enderror" disabled>
                                            @error('coMaker1_years_of_employment')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <!-- =============== Co-Maker Details (2) =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Co-Maker Details (2)</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('coMaker2_last_name', app('request')->input('coMaker2_last_name')) }}" name="coMaker2_last_name" class="form-control @error('coMaker2_last_name') is-invalid @enderror" disabled>
                                            @error('coMaker2_last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('coMaker2_first_name', app('request')->input('coMaker2_first_name')) }}" name="coMaker2_first_name" class="form-control @error('coMaker2_first_name') is-invalid @enderror" disabled>
                                            @error('coMaker2_first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('coMaker2_middle_name', app('request')->input('coMaker2_middle_name')) }}" name="coMaker2_middle_name" class="form-control @error('coMaker2_middle_name') is-invalid @enderror" disabled>
                                            @error('coMaker2_middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" placeholder="Email Address" name="coMaker2_email_address" class="form-control @error('coMaker2_email_address') is-invalid @enderror" disabled>
                                            @error('coMaker2_email_address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." name="coMaker2_contact_no" class="form-control @error('coMaker2_contact_no') is-invalid @enderror" disabled>
                                            @error('coMaker2_contact_no')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Name of Employer <star class="star">*</star></label>
                                            <input type="email" placeholder="Name of Employer" name="coMaker2_employer_name" class="form-control @error('coMaker2_employer_name') is-invalid @enderror" disabled>
                                            @error('coMaker2_employer_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Position <star class="star">*</star></label>
                                            <input type="text" placeholder="Position" name="coMaker2_position" class="form-control @error('coMaker2_position') is-invalid @enderror" disabled>
                                            @error('coMaker2_position')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="coMaker2_employment_status" class="text-body">Employment Status <star class="star">*</star></label>
                                        <div class="row" id="coMaker2_employment_status">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Probitionary
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Regular
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Contractual">
                                                    <span class="form-radio-sign"></span>
                                                    Contractual
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="coMaker2_employment_status" value="Project-Based">
                                                    <span class="form-radio-sign"></span>
                                                    Project-Based
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Years of Employment <star class="star">*</star></label>
                                            <input type="number" placeholder="Years of Employment" name="coMaker2_years_of_employment" class="form-control @error('coMaker2_years_of_employment') is-invalid @enderror" disabled>
                                            @error('coMaker2_years_of_employment')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <!-- =============== Credit Evaluation and Recommendation =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-light" style="background: gray;"><strong>Credit Evaluation and Recommendation</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card table-with-links">
                                            <div class="card-body table-responsive">
                                                <form method="GET" action="{{ url('loans/patients') }}">
                                                    @csrf
                                                    <table class="table table-hover">
                                                        <caption class="text-center text-dark" style="caption-side: top;background:#AED6F1;">COOP RELATIONS</caption>
                                                        <thead>
                                                            <th class="text-left">Existing Loans</th>
                                                            <th>Amount</th>
                                                            <th>Loan Balance</th>
                                                            <th class="text-right">Amortization</th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><input type="text" name="first_name" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="number" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="number" name="middle_name" value="demo" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="number" name="contact_number" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td><textarea type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Total Loan Deductions</textarea></td>
                                                                <td><input type="number" name="schedule_date" value="100" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="number" name="middle_name" value="100" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="number" name="contact_number" value="100" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card table-with-links">
                                            <div class="card-body table-responsive">
                                                <form method="GET" action="{{ url('loans/patients') }}">
                                                    @csrf
                                                    <table class="table table-hover">
                                                        <caption class="text-center text-dark" style="caption-side: top;background:#AED6F1;">EXISTING DEPOSITS</caption>
                                                        <thead>
                                                            <th class="text-left">Existing Deposits</th>
                                                            <th class="text-center">Balance</th>
                                                            <th class="text-right">Amortization</th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Paid-up Share Capital</textarea></td>
                                                                <td><input type="number" name="schedule_date" value="demo" class="form-control bg-transparent border-white text-center" disabled></input></td>
                                                                <td><input type="number" name="middle_name" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Special Savings</textarea></td>
                                                                <td><input type="number" name="schedule_date" value="demo" class="form-control bg-transparent border-white text-center" disabled></input></td>
                                                                <td><input type="number" name="middle_name" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Savings Deposit</textarea></td>
                                                                <td><input type="number" name="schedule_date" value="demo" class="form-control bg-transparent border-white text-center" disabled></input></td>
                                                                <td><input type="number" name="middle_name" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td><textarea type="text" name="first_name" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Total Deposits</textarea></td>
                                                                <td><input type="number" name="schedule_date" value="100" class="form-control bg-transparent border-white text-center" disabled></input></td>
                                                                <td><input type="number" name="middle_name" value="100" class="form-control bg-transparent border-white text-right"  disabled></input></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card table-with-links">
                                            <div class="card-body table-responsive">
                                                <form method="GET" action="{{ url('loans/patients') }}">
                                                    @csrf
                                                    <table class="table table-hover">
                                                        <caption class="text-center text-dark" style="caption-side: top;background:#AED6F1;">LOAN COMPUTATION</caption>
                                                        <thead>
                                                            <th class="text-left"></th>
                                                            <th></th>
                                                            <th>Semi-</th>
                                                            <th class="text-right">Monthly</th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Basic Pay</textarea></td>
                                                                <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="text" name="middle_name" value="demo" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="text" name="contact_number" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Allowable Amort.</textarea></td>
                                                                <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="text" name="middle_name" value="demo" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="text" name="contact_number" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea rows="1" cols="50" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Non-coop Deductions</textarea></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>SSS Loan</textarea></td>
                                                                <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="text" name="middle_name" value="demo" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="text" name="contact_number" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Pag-IBIG Loan</textarea></td>
                                                                <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="text" name="middle_name" value="demo" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="text" name="contact_number" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;"></textarea></td>
                                                                <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="text" name="middle_name" value="demo" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="text" name="contact_number" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;"></textarea></td>
                                                                <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="text" name="middle_name" value="demo" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="text" name="contact_number" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;"></textarea></td>
                                                                <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="text" name="middle_name" value="demo" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="text" name="contact_number" value="demo" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td><textarea type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Total Non-coop Deductions</textarea></td>
                                                                <td><input type="text" name="schedule_date" value="100" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="text" name="middle_name" value="100" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="text" name="contact_number" value="100" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Allowable for New Accomodation</textarea></td>
                                                                <td><input type="text" name="schedule_date" value="100" class="form-control bg-transparent border-white" disabled></input></td>
                                                                <td><input type="text" name="middle_name" value="100" class="form-control bg-transparent border-white"  disabled></input></td>
                                                                <td><input type="text" name="contact_number" value="100" class="form-control bg-transparent border-white text-right" disabled></input></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card table-with-links">
                                            <div class="card-body table-responsive">
                                                <form method="GET" action="{{ url('loans/patients') }}">
                                                    @csrf
                                                    <table class="table table-hover">
                                                        <caption class="text-center text-dark" style="caption-side: top;background:#AED6F1;">Computation of Loanable Amount</caption>
                                                        <tbody>
                                                            <tr>
                                                                <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Net Allowed for Amort.</textarea></td>
                                                                <td><input type="text" name="net_allowed_for_amort_value" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea type="text" rows="1" name="coop_loan_deduction" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Less: Coop Loan Deduction</textarea></td>
                                                                <td><input type="text" name="coop_loan_deduction" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea type="text" rows="1" name="coop_deposit_deduction" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Less: Coop Deposit Deduction</textarea></td>
                                                                <td><input type="text" name="coop_deposit_deduction_value" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea type="text" rows="1" name="net_allowed_for_new_amort" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Net Allowed for New Amort.</textarea></td>
                                                                <td><input type="text" name="net_allowed_for_new_amort" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea type="text" rows="1" name="max_loanable_amount" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Maximum Loanable Amount</textarea></td>
                                                                <td><input type="number" name="max_loanable_amount_value" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td><textarea type="text" rows="1" name="rounded_loanable_amount" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Rounded Loanable Amount</textarea></td>
                                                                <td><input type="number" name="rounded_loanable_amount_value" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="dosri" name="dosri" value="dosri">&nbsp;&nbsp;
                                            <label for="dosri"> DOSRI</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="card table-with-links">
                                            <div class="card-body table-responsive">
                                                <table class="table table-hover">
                                                    <caption class="text-center text-dark" style="caption-side: top;background:#AED6F1;">RECOMMENDATION</caption>
                                                    <tbody>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Loan Amount</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Term</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Interest Rate</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Ins. Type</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>TOTALS</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="100" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Amortization</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="100" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card table-with-links">
                                            <div class="card-body table-responsive">
                                                <table class="table table-hover">
                                                    <caption class="text-center text-dark" style="caption-side: top;background:#AED6F1;">New Structure of Deduction</caption>
                                                    <tbody>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Existing Loan Amort.</textarea></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="coop_loan_deduction" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Existing Deposit Amort.</textarea></td>
                                                            <td><input type="text" name="coop_loan_deduction" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" cols="30" name="coop_deposit_deduction" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>New Loan Amort.</textarea></td>
                                                            <td><input type="text" name="coop_deposit_deduction_value" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_new_amort" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Total Amortization</textarea></td>
                                                            <td><input type="text" name="net_allowed_for_new_amort" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="max_loanable_amount" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Max Capacity</textarea></td>
                                                            <td><input type="number" name="max_loanable_amount_value" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="rounded_loanable_amount" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled>Excess/Deficiency</textarea></td>
                                                            <td><input type="number" name="rounded_loanable_amount_value" value="1" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="card table-with-links">
                                            <div class="card-body table-responsive">
                                                <table class="table table-hover">
                                                    <caption class="text-center text-dark" style="caption-side: top;background:#AED6F1;">Terms and Condition</caption>
                                                    <tbody>
                                                        <tr>
                                                            <td><textarea type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;"></textarea></td>
                                                        </tr>
                                                </table>
                                                <table class="table table-hover">
                                                    <caption class="text-center text-dark" style="caption-side: top;background:#AED6F1;">Considerations</caption>
                                                    <tbody>
                                                        <tr>
                                                            <td><textarea type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;"></textarea></td>
                                                        </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card table-with-links">
                                            <div class="card-body table-responsive">
                                                <table class="table table-hover">
                                                    <caption class="text-center text-dark" style="caption-side: top;background:#AED6F1;">LOAN COMPUTATION SUMMARY</caption>
                                                    <tbody>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Loan Amount</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Less: Service Charge</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Less: Special Savings</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Less: Loan Deductions</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea rows="1" type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>Less: Other Deductions</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td><textarea type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>TOTAL Deductions</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="100" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" name="first_name" class="form-control bg-transparent border-white" style="resize:none;" disabled>NET PROCEEDS</textarea></td>
                                                            <td><input type="text" name="schedule_date" value="100" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="schedule_date" value="demo" class="form-control bg-transparent border-white" disabled></input></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="release_mode" class="text-body">Mode of Releasing: <star class="star">*</star></label>
                                        <div class="row" id="release_mode">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="release_mode" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Cash
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" name="release_mode" value="Regular">
                                                    <span class="form-radio-sign"></span>
                                                    Cheque
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row" id="release_mode">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="release_mode" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Credit to GCash Account
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row" id="release_mode">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="release_mode" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Credit to LIPIEMCO Savings
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row" id="release_mode">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="release_mode" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Credit to Online UBP
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row" id="release_mode">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" name="release_mode" value="Probitionary">
                                                    <span class="form-radio-sign"></span>
                                                    Credit to Online BPI
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row col-md-4">
                                            <div class="form-group">
                                                <input type="number" style="width: 250px;" placeholder="Account Number" name="account_number" class="form-control @error('account_number') is-invalid @enderror" disabled>
                                                @error('account_number')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <h4 class="card-title">Attached Files</h4>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-6" >
                                        <div class="card w-75 h-100  border border-danger" style="width: 18rem; background-color: #ffffb3">
                                            <div class="text-center font-weight-bold mt-2"> Required Forms</div>
                                            <div class="text-center">
                                                @if(session()->has('screeningFormData'))
                                                    <a class="btn" href="{{ url ('loans/personaldisclosureform') }}" data-form-screen style="border:none; background-color: #ffffb3; color:green">
                                                        <strong>1. Personal Data Disclosure Authorization Form</strong>        
                                                    </a><i class="fa fa-check-square-o"></i>
                                                @else
                                                    <a class="btn" href="{{ url ('loans/personaldisclosureform') }}" data-form-screen style="border:none; background-color: #ffffb3; color:blue">
                                                        <strong>1. Personal Data Disclosure Authorization Form</strong>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="text-center">
                                                @if(session()->has('consentFormData'))
                                                    <a class="btn" href="{{ url('loans/pledgedepositform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                        <strong>2. Pledge of Deposit Form</strong>
                                                    </a><i class="fa fa-check-square-o"></i>
                                                @else
                                                    <a class="btn" href="{{ url('loans/pledgedepositform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                        <strong>2. Pledge of Deposit Form</strong>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="text-center">
                                                @if(session()->has('consentFormData'))
                                                    <a class="btn" href="{{ url('loans/authoritydeductform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                        <strong>3. Authority to Deduct Form</strong>
                                                    </a><i class="fa fa-check-square-o"></i>
                                                @else
                                                    <a class="btn" href="{{ url('loans/authoritydeductform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                        <strong>3. Authority to Deduct Form</strong>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="text-center">
                                                @if(session()->has('consentFormData'))
                                                    <a class="btn" href="{{ url('loans/promissorynoteform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                        <strong>4. Promissory Note Form</strong>
                                                    </a><i class="fa fa-check-square-o"></i>
                                                @else
                                                    <a class="btn" href="{{ url('loans/promissorynoteform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                        <strong>4. Promissory Note Form</strong>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card table-with-links">
                                            <div class="card-body table-responsive">
                                                <table class="table table-hover">
                                                    <caption class="text-center text-dark" style="caption-side: top;background:#AED6F1;">Reviews and Approvals</caption>
                                                    <thead>
                                                            <th class="text-left">Date</th>
                                                            <th>Time</th>
                                                            <th>Process Step</th>
                                                            <th>By</th>
                                                            <th>Status</th>
                                                            <th>Remarks</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled></textarea></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="CMG Evaluation" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;"></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled></textarea></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="Loans Head Review" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;"></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled></textarea></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="GM Approval" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;"></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled></textarea></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="CreCom Approval" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;"></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled></textarea></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="BOD Evaluation" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;"></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;" disabled></textarea></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="Cashier Disbursement" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><input type="text" name="net_allowed_for_amort_value" value="" class="form-control bg-transparent border-white" disabled></input></td>
                                                            <td><textarea type="text" rows="1" name="net_allowed_for_amort" class="form-control bg-transparent border-white text-left" style="resize:none;"></textarea></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                            <div class="card-footer ">
                                <button type="button" class="btn btn-fill btn-primary" disable>Submit</button>
                                <a href="{{ url('loans/create') }}" class="btn btn-fill btn-danger">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {

            
        });

    </script>

@endsection