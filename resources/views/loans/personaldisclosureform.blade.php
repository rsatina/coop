@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                <i class="nc-icon nc-simple-remove"></i>
                            </button>
                            <span>
                                <b>
                                    {{ session('message') }}
                                </b>
                            </span>
                        </div>
                    @endif
                    @if ($errors->any())
                        <ul>
                        @foreach ($errors->all() as $message)
                            <li> {{ $message }} </li>
                        @endforeach
                        </ul>
                    @endif
                    <form method="GET" action="">
                        @csrf
                        <input type="hidden" name="schedule_date">
                        <input type="hidden" name="schedule_time">
                        <input type="hidden" name="requirements" value="{{ app('request')->input('requirements') }}">
                        <input type="hidden" name="middle_name" value="{{ app('request')->input('middle_name') }}">
                        <input type="hidden" id="birthdate" name="birthdate" value="{{ app('request')->input('birthdate') }}">
                        <div class="card stacked-form">
                            <div class="card-header text-center" style="background-color:green">
                                <h4 class="card-title text-light pb-2" ><strong>Personal Data Disclosure Authorization<strong></h4>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-12 pt-3">
                                        <div class="form-group font-weight-normal">
                                            <p class="text-justify">I hereby give consent to LIPIEMCO to collect and gather my personal data, to store and
                                            to process such data for the purpose of availing products and services from LIPIEMCO,
                                            and sharing such data to Philippine Cooperative Central Fund Federation, for its
                                            iMCOOP and for coop analytics.
                                            </p>
                                            </br>
                                            <p class="text-justify">I hereby acknowledge and authorize:
                                                <ol>
                                                    <li class="text-justify">the regular submission and disclosure of my basic credit data (as defined
                                                    under Republ ic Act No. 9510 and its Implementing Rules and Regulations) to the Credit
                                                    Information Corporation (CIC) as well as any updates or correc tions thereof; and
                                                    </li>
                                                    <li class="text-justify">the sharing of my basic credit data with other lenders authorized by the CIC,
                                                    and credit reporting agencies d uly accredited by the CIC. I further consent the pulling
                                                    and disclosure of my credit report and score from the CIC and CIBI Information Inc to
                                                    establish my creditworthiness and in relation to my loan application and management.
                                                    </li>
                                                </ol>
                                            </p>
                                            </br>
                                            <p class="text-justify">I further authorize the use of my credit data and report by CIBI Information for Coop
                                            Analytics and Credit Bureau purposes.
                                            </p>
                                            </br>
                                            <p>Name of Borrower: <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width:250px;"></p>
                                            <p>Loan Application No.: <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width:230px;"></p>
                                            <p>Date of Disclosure: <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width:250px;"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer justify-content-right">
                                <button type="submit" name="action" class="btn btn-fill btn-primary" value="saveConsentForm">Save</button>
                                <a href="javascript:history.back()" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            var nowDate = new Date().getFullYear();
            var bDate = new Date($('#birthdate').val()).getFullYear();
            var a = nowDate - bDate;
            $('#age').val(a);
        });

    </script>
@endsection