@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                <i class="nc-icon nc-simple-remove"></i>
                            </button>
                            <span>
                                <b>
                                    {{ session('message') }}
                                </b>
                            </span>
                        </div>
                    @endif
                    @if ($errors->any())
                        <ul>
                        @foreach ($errors->all() as $message)
                            <li> {{ $message }} </li>
                        @endforeach
                        </ul>
                    @endif
                    <form method="GET" action="">
                        @csrf
                        <input type="hidden" name="schedule_date">
                        <input type="hidden" name="schedule_time">
                        <input type="hidden" name="requirements" value="{{ app('request')->input('requirements') }}">
                        <input type="hidden" name="middle_name" value="{{ app('request')->input('middle_name') }}">
                        <input type="hidden" id="birthdate" name="birthdate" value="{{ app('request')->input('birthdate') }}">
                        <div class="card stacked-form">
                            <div class="card-header text-center" style="background-color:green">
                                <h4 class="card-title text-light pb-2" ><strong>Authority To Deduct<strong></h4>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-12 pt-3">
                                        <div class="form-group font-weight-normal">
                                            <p class="text-justify">I, <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;">, with ID no. <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;" required>  hereby authorize the Finance/HR Department of <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;">  to deduct from my salary the amount due me of this loan in semi-monthly installments until its full payment and remit the same to LIPIEMCO this coming pay period.</p>
                                            </br>
                                            <p class="text-justify">In case of my separation or resignation, I hereby authorize LIPIEMCO to get the proceeds of my resignation or separation pay as payment of my obligations in LIPIEMCO.</p>
                                            </br>
                                            <p>Name of Borrower: <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width:250px;"></p>
                                            <p>Loan Application No.: <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width:230px;"></p>
                                            <p>Date of Disclosure: <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;width:250px;"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer justify-content-right">
                                <button type="submit" name="action" class="btn btn-fill btn-primary" value="saveConsentForm">Save</button>
                                <a href="javascript:history.back()" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            var nowDate = new Date().getFullYear();
            var bDate = new Date($('#birthdate').val()).getFullYear();
            var a = nowDate - bDate;
            $('#age').val(a);
        });

    </script>
@endsection