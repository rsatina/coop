@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="">
                        @csrf
                        <div class="card">
                            @if ($errors)
                                <div class="card-header">
                                    <p class="error" style="color:red">{{ $errors->first() }}</p>
                                </div>
                            @endif
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Loan Details</strong></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="loan_type" class="text-body">Type of Loan <star class="star">*</star></label>
                                        <div class="row" id="loan_type">
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body" style="margin-left: -5px;">
                                                    <input class="form-radio-input" type="radio" disabled <?php echo (($comaker->first()->loan_type == "Providential Loan") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Providential Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" disabled <?php echo (($comaker->first()->loan_type == "Express Loan") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Express Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" disabled <?php echo (($comaker->first()->loan_type == "Instant Loan") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Instant Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" disabled <?php echo (($comaker->first()->loan_type == "Seasonal Loan") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Seasonal Loan
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" disabled <?php echo (($comaker->first()->loan_type == "Business Loan") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Business Loan
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p></p>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Date </label>
                                            <div class='input-group date' id='loan-date'>
                                                <input type='text' id="loandate-picker" class="form-control datepicker @error('loandate-picker') is-invalid @enderror" name="loan_date" placeholder="YYYY-MM-DD" disabled/>
                                                <label class="input-group-append input-group-text" for="loandate-picker" style="margin:inherit;border-radius:1px;">
                                                    <span class="fa fa-calendar"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Loan Application No: </label>
                                            <input type="text" name="loan_application_no" value="{{ $comaker->first()->loan_application_no }}" style="border-style: none none none none;">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Amount Applied <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->amount_word }}" class="form-control" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Php <star class="star">*</star></label>
                                            <input type="number" value="{{ $comaker->first()->amount_figure }}" class="form-control" disabled/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Purpose <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->purpose }}" class="form-control" style="resize: none;" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Interest Rate <star class="star">*</star></label>
                                            <input type="number" value="{{ $comaker->first()->interest_rate }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="text-body">Terms Applied<star class="star">*</star></label>
                                                <input type='text' class="ml-1" style="border-style: none none solid none;width:50px;font-size:15px" value="{{ $comaker->first()->term_applied  }}" name="term_applied" disabled> months
                                        </div>
                                    </div>
                                    <div class="col-md-7" style="font-size:12px">
                                        <div class="row" id="payment_mode">
                                            MODE OF PAYMENT:
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" disabled <?php echo (($comaker->first()->mode_of_payment == "Post Dated Check") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Post Dated Check
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" disabled <?php echo (($comaker->first()->mode_of_payment == "Payroll Deduction") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Payroll Deduction
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="text-body">
                                                    <input class="form-radio-input" type="radio" disabled <?php echo (($comaker->first()->mode_of_payment == "Cash") ? 'checked' : '');?>>
                                                    <span class="form-radio-sign"></span>
                                                    Cash
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <p></p>

                                <!-- =============== Member Borrower's Details =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Borrower Details</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->lastname }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->firstname }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" value="{{ $comaker->first()->middlename }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" value="{{ $comaker->first()->borrower_email }}"class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" value="{{ $comaker->first()->contact_no }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">Present Address <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->present_address }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">Provincial Address <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->provincial_address }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Name of Employer <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->employer_name }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Position <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->position }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">Employer's Address <star class="star">*</star></label>
                                            <input type="text" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <!-- =============== Co-Maker Details =============== -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Co-Maker Details</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->cm_lastname }}" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->cm_firstname }}" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" value="{{ $comaker->first()->cm_middlename }}" class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Email Address <star class="star">*</star></label>
                                            <input type="email" value="{{ $comaker->first()->comaker_email }}" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact No. <star class="star">*</star></label>
                                            <input type="number" value="{{ $comaker->first()->cm_contact_no }}"class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">Present Address <star class="star">*</star></label>
                                            <input type="text" placeholder="Present Address" name="present_address" class="form-control @error('borrower_present_address') is-invalid @enderror" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">Provincial Address <star class="star">*</star></label>
                                            <input type="text" placeholder="Provincial Address" name="provincial_address" class="form-control @error('provincial_address') is-invalid @enderror" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-body">Name of Employer <star class="star">*</star></label>
                                            <input type="email" value="{{ $comaker->first()->cm_employer_name }}" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Position <star class="star">*</star></label>
                                            <input type="text" value="{{ $comaker->first()->cm_position }}" class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-body">Employer's Address <star class="star">*</star></label>
                                            <input type="text" placeholder="Employer's Address" name="employers_address" class="form-control @error('employers_address') is-invalid @enderror" required>
                                        </div>
                                    </div>
                                </div>
                                <p></p>

                                <!-- Promissory Note-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Promissory Note</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <p></p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <p>Date of Note: <input class="form" type="text"  style="border-style: none none solid none;"></p>
                                            <p>Maturity Date: <input class="form" type="text" style="border-style: none none solid none;width:195px"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <p>PN Number : <input class="form" placeholder="" type="text" style="border-style: none none solid none;"></p>
                                    </div>
                                </div>
                                <p></p>
                                <div class="row">
                                    <div class="col-md-12 pt-3">
                                        <div class="form-group font-weight-normal">
                                            <p class="text-justify">For VALUE RECEIVED, I/We promise to pay, jointly and severally to the order of LIPI EMPLOYEES MULTIPURPOSE COOPERATIVE (LIPIEMCO) the sum in PESOS:
                                            </br><input class="form" placeholder="Amount in Words" type="text" style="border-style: none none solid none;width: 500px;">
                                            (Php <input class="form" placeholder="Amount in Numbers" type="text" style="border-style: none none solid none;"> ),
                                            plus interest in the rate of <input class="form" type="text" style="border-style: none none solid none;"> %
                                            per annum, payable in installments of <input class="form" type="text" placeholder="Amount in Words" style="border-style: none none solid none;width:500px;">
                                            (Php <input class="form" type="text" placeholder="Amount in Numbers" style="border-style: none none solid none;"> )
                                            the first payment to be made on <input class="form" type="text" style="border-style: none none solid none;">
                                            and a like amount every installment period until the full amount is fully paid.
                                            </p>
                                            
                                            </br>
                                            <p class="text-justify">The obligation contracted in this Note is resulting from a loan granted by LIPIEMCO to me/us. 
                                            This note is subject to a Real Estate or Chattel Mortgage, if applicable, to be executed by me/us 
                                            to secure this loan. Failure on my part to apply the said amount to the purpose for which it is 
                                            granted would render the whole amount of the loan due and demandable. In case of any failure or
                                            default in payments as herein agreed or failure on my/our part to make prompt payment on any
                                            of the monthly stipulated due dates shall render the entire amount of loan or the balance thereof
                                            due and demandable. Each party to this note whether as a maker, co-maker, endorser or
                                            guarantor; severally waive presentation of payment, demand protest and notice of protest of the
                                            same. Any partial payment or performance of this note or any extension thereof granted shall
                                            neither alter, novate nor vary the original terms of the obligation not discharge, and such partial
                                            payment of performance shall be considered as written acknowledgement of this obligation
                                            which interrupts the period of prescription.
                                            </p>
                                            </br>
                                            <p class="text-justify">To further secure this loan, I hereby pledge and assign to LIPIEMCO my salary and wage, 
                                            savings/time Deposits/Share Capital, patronage refunds and interest on Share Capital. In case of
                                            my separation or resignation from <input class="form" type="text" style="border-style: none none solid none;width: 500px;">
                                            employment before full payment of this loan, I likewise pledge and assign my retirement or
                                            separation pay, pension including but not limited to our advance pension, if any. For this
                                            purpose, I hereby appoint and designate with the understanding not to revoke this appointment/
                                            designation without written authority of LIPIEMCO, my employer or attorney-in-fact, to pay the
                                            loan by deducting or collecting from any or all of the aforementioned income due to me in such
                                            amount as to satisfy the principal, interest and penalties, if any, on said loan.
                                            </p>
                                            </br>
                                            <p class="text-justify">It is further agreed that in case payment shall not be made at maturity, I shall pay a penalty for
                                            2% per month plus collection cost, and attorney’s fees in addition to the principal and interest
                                            due on this note. Such charge in no event to be less than ONE THOUSAND PESOS (php
                                            1,000.00) and that said suit shall be subject to the jurisdiction of the courts of Cebu City, or 
                                            Mandaue City.
                                            </p>
                                            </br>
                                            <p class="text-justify">In witness whereof, I/We hereunto signed this instrument this <input class="form" type="text" style="border-style: none none solid none;">
                                            day of <input class="form" type="text"  style="border-style: none none solid none;">
                                            at <input class="form" type="text"  style="border-style: none none solid none;">,
                                            Philippines.
                                            </p>
                                            <p>Co-Makers Name: <input class="form" type="text"  style="border-style: none none solid none;width:250px;"></p>
                                            <p>ID Attached: <input class="form" type="text" style="border-style: none none solid none;width:245px;"></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row" id="terms_conditions">
                                                <div class="form-check checkbox-inline">
                                                    <label class="form-check-label text-body" style="font-size: 17px">
                                                        <input class="form-check-input" type="checkbox" name="terms_conditions">
                                                        <span class="form-check-sign"></span>
                                                        <strong>I have read, understood and agree to these terms and conditions.</strong>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div></br>

                                <!-- Authority to deduct -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="form-control text-center text-dark" style="background: #DCDCDC;font-size:17px"><strong>Co-Maker Statement With Authority To Deduct</strong></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 pt-3">
                                        <div class="form-group font-weight-normal">
                                            <p class="text-justify">I, 
                                                <input class="form" type="text" style="border-style: none none solid none;">, 
                                                agree to become the CO-MAKER of <input class="form" type="text" style="border-style: none none solid none;" required>  
                                                voluntarily  signed and submit this co-maker statement for the loan amount not exceeding <input class="form" type="text" style="border-style: none none solid none;">
                                                (Php <input class="form" type="text" style="border-style: none none solid none;"> )
                                                for the period of <input class="form" type="text" style="border-style: none none solid none;"> months.
                                            </p>
                                            <p class="text-justify">I further agree to voluntarily and willingly bind  myself to pay jointly and severally  or solidarity all his/her unpaid obligations to
                                                LIPIEMCO, according to the terms and conditions of Promissory Note which I agreed to in case he/she fails to pay his/her
                                                obligations for whatever reason.
                                            </p>
                                            <p class="text-justify">With this, I hereby authorized <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;">
                                                to deduct from my salaries, bonuses, separation pay, final salary,
                                                retirement benefits or whatever monetary grants the amount due including interest and penalties at the request of LIPIEMCO if
                                                this loan is not paid by the Borrower.
                                            </p>
                                            <p class="text-justify">Furthermore, I hereby permit the LIPIEMCO to withhold the release og my Share Capital contribution until such time the
                                                principal borrower has found my replacement as Co-Maker or has paid in full his/her obligation.
                                            </p>
                                            <p class="text-justify">Upon my membership termination, I hereby authorize LIPIEMCO to offset the borrower's outstanding obligation from my share
                                            capital contribution and last claims.
                                            </p>
                                            <p class="text-justify">This authorization shall be effective until full payment of the loan herein contracted.</p>
                                            <p class="text-justify">I have attached a copy of my valid ID in lieu of my signature and as proof that I have read, understood, agreed and commit myself to the aforementioned conditions.</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row" id="terms_conditions">
                                                <div class="form-check checkbox-inline">
                                                    <label class="form-check-label text-body" style="font-size: 17px">
                                                        <input class="form-check-input" type="checkbox" name="terms_conditions">
                                                        <span class="form-check-sign"></span>
                                                        <strong>I have read, understood and agree to these terms and conditions.</strong>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <hr>

                                <h4 class="card-title">Attached Files</h4>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-6" >
                                    </div>
                                </div>
                            </div>
                    
                            <div class="card-footer ">
                                <button type="submit" name="action" class="btn btn-fill btn-success" value="create">Submit</button>
                                <a href="{{ url()->previous() }}" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            // date today
            //var date = new Date().toISOString().slice(0,10);

            // Date picker set
            //var loandatePicker = $("#loandate-picker");

            //loandatePicker.datetimepicker({
            //    viewMode: "years",
            //    format: 'YYYY-MM-DD',
            //    minDate: moment().format("YYYY-MM-DD")
            //});

            //$("#loandate-picker").val(date);
        });
    </script>

@endsection