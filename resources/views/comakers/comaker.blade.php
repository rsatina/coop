@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                <i class="nc-icon nc-simple-remove"></i>
                            </button>
                            <span>
                                <b>
                                    {{ session('message') }}
                                </b>
                            </span>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-4 ml-0">
                            <div class="form-group">
                                <label class="text-body">Loan Application No.</label>
                                <input type="number" placeholder="Loan Application No." name="loan_application_no" class="form-control @error('loan_application_no') is-invalid @enderror">
                            </div>
                        </div>
                    </div>
                    <div class="card table-with-links">
                        <div class="card-body table-responsive">
                        
                            @if ($errors->any())
                            <ul> 
                                @foreach ($errors->all() as $message)
                                    <li> {{ $message }}</li>
                                @endforeach
                            </ul>
                            @endif
                            
                            <table class="table table-hover table-striped table-bordered">
                                <thead>
                                <tr class="d-flex" style="background-color:#182370">
                                    <th class="text-center col-2 text-white"><strong>Loan Application No.</strong></th>
                                    <th class="text-center col-2 text-white"><strong>Date Submitted</strong></th>
                                    <th class="text-center col-2 text-white"><strong>Full Name</strong></th>
                                    <th class="text-center col-2 text-white"><strong>Type of Loan</strong></th>
                                    <th class="text-center col-1 text-white"><strong>Loan Amount</strong></th>
                                    <th class="text-center col-2 text-white"><strong>Status</strong></th>
                                    <th class="text-center col-1 text-white"><strong>Actions</strong></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($comakers as $key => $comaker)
                                    <tr class="d-flex">
                                        <form method="post" action="">
                                        @csrf
                                            <td class="text-center col-2">{{ $comaker->loan_application_no }}</td>
                                            <td class="text-center col-2">{{ date('M-d-Y h:i A', strtotime($comaker->created_at)) }}</td>
                                            <td class="text-center col-2">{{ $comaker->firstname }} {{ $comaker->lastname }}</td>
                                            <td class="text-center col-2">{{ $comaker->loan_type }}</td>
                                            <td class="text-center col-1">{{ $comaker->amount_figure }}</td>
                                            <td class="text-center col-2">
                                                @if(auth()->user()->isAdministrator())
                                                    <select name="status" onchange='this.form.submit()' style="border:none;color:#888888" class="text-body">
                                                        <option value='1'>For Verification</option>
                                                        <option value='2'>Verified</option>
                                                        <option value='3'>For Evaluation</option>
                                                        <option value='4'>Loans Head Review</option>
                                                        <option value='5'>GM Approval</option>
                                                        <option value='5'>CreCom Approval</option>
                                                        <option value='5'>BOD Approval</option>
                                                        <option value='5'>Cashier Disbursement</option>
                                                        <option value='5'>Released/Deposited</option>
                                                        <option value='5'>Lacking Requirements</option>
                                                        <option value='5'>On-Hold</option>
                                                        <option value='5'>Cancelled</option>
                                                        <option value='5'>Denied</option>
                                                    </select>
                                                @else
                                                    {{ $comaker->status }}
                                                @endif
                                            </td>
                                            <td class="col-1" style='font-size:13px'>
                                                <a href="{{ url('comakers/showcomaker', ['id' => $comaker->id]) }}">- View</a><br>
                                                <a href="">- Accept</a><br>
                                                <a href="">- Decline</a>
                                            </td>
                                        </form>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('javascript')
    <script type="text/javascript">
    jQuery.fn.extend({
      autoHeight: function () {
        function autoHeight_(element) {
          return jQuery(element).css({
            'height': 'auto',
            'overflow-y': 'hidden'
          }).height(element.scrollHeight);
        }
        return this.each(function () {
          autoHeight_(this).on('input', function () {
            autoHeight_(this);
          });
        });
      }
    });
    $('#covid_form').autoHeight();
    </script>
@endsection