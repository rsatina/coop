<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'LIPIEMCO') }}</title>

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />

    <!-- CSS Files -->
    <link href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/datetimepicker.css') }}" rel="stylesheet" />

    @yield('stylesheets')

    <link href="{{ asset('css/theme.css') }}" rel="stylesheet" />
</head>

<body>
    <div class="wrapper">

        @section('sidebar')
            <div class="sidebar" data-color="azure" data-image="https://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/img/sidebar-5.jpg">
            <!--
            Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

            Tip 2: you can also add an image using data-image tag
            -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="https://www.tigdesignsolutions.com/" target="_blank" class="logo-mini">
                        <img src="{{asset('images/lipiemco-logo.png')}}" width="50" height="50"/>
                    </a><br><br>
                </div>
                <div class="user">
                    <div class="photo">
                        <img src="https://cdn.pixabay.com/photo/2020/04/15/17/32/medical-5047586_960_720.png" />
                    </div>
                    <div class="info ">
                        <a data-toggle="collapse" href="#" class="collapsed">
                                <span>{{ auth()->user()->name }}</span>
                        </a>
                    </div>
                </div>
                <ul class="nav">
                    <li class="nav-item {{request()->routeIs('loans.create') ? 'active' : ''}}">
                        <a class="nav-link" href="{{ url('loans/create') }}">
                            <i class="nc-icon nc-single-copy-04"></i>
                            <p>Loan Application</p>
                        </a>
                    </li>
                    <li class="nav-item {{request()->routeIs('withdrawals.savingswithdrawal') ? 'active' : ''}}">
                        <a class="nav-link" href="{{ url('withdrawals/savingswithdrawal') }}">
                            <i class="nc-icon nc-bank"></i>
                            <p>Savings Withdrawal</p>
                        </a>
                    </li>
                    {{--@if(auth()->user()->isAdministrator())
                    @else--}}
                        <li class="nav-item {{request()->routeIs('transactions.transactions') ? 'active' : ''}}">
                            <a class="nav-link" href="{{ url('transactions/transactions') }}">
                                <i class="nc-icon nc-money-coins"></i>
                                <p>My Transactions</p>
                            </a>
                        </li>
                        <li class="nav-item {{request()->routeIs('comakers.comaker') ? 'active' : ''}}">
                            <a class="nav-link" href="{{ url('comakers/comaker') }}">
                                <i class="nc-icon nc-badge"></i>
                                <p>Co-Maker</p>
                            </a>
                        </li>
                    {{--@endif--}}
                    </br>
                    @if(request()->routeIs('loans.create'))
                    {{--@if(!auth()->user()->isAdministrator())--}}
                    <div class="container" style="background-color: #ccffff">
                        <div class="text-danger font-weight-bold text-center"> REQUIREMENTS </div>
                        <p></p>
                        <div class="text-dark" style="font-size: 12px; text-align:justify;">
                            <strong><b>1.</b> Completely filled-out Loan Application Form</strong>
                        </div>
                        <p></p>
                        <div class="text-dark" style="font-size: 12px; text-align:justify;">
                            <strong><b>2.</b> <a href="{{ url ('loans/personaldisclosureform') }}"> Personal Data Disclosure Authorization Form</strong>
                        </div>
                        <p></p>
                        <div class="text-dark" style="font-size: 12px; text-align:justify;">
                            <strong><b>3.</b> <a href="{{ url ('loans/pledgedepositform') }}"> Pledge of Deposit Form </a></strong>
                        </div>
                        <p></p>
                        <div class="text-dark" style="font-size: 12px; text-align:justify;">
                            <strong><b>4.</b> <a href="{{ url ('loans/authoritydeductform') }}"> Authority to Deduct Form </a></strong>
                        </div>
                        <p></p>
                        <div class="text-dark" style="font-size: 12px; text-align:justify;">
                            <strong><b>5.</b> <a href="{{ url ('loans/promissorynoteform') }}"> Promissory Note Form </a></strong>
                        </div>
                        <p></p>
                        <div class="text-dark" style="font-size: 12px; text-align:justify;">
                            <strong><b>6.</b> Copy of company ID or government-issued ID</strong>
                        </div>
                        <p></p>
                        <div class="text-dark" style="font-size: 12px; text-align:justify;">
                            <strong><b>7.</b> Sketch of residence</strong>
                        </div>
                        <p></p>
                        <div class="text-dark" style="font-size: 12px; text-align:justify;">
                            <strong><b>8.</b> Copy of last 2 consecutive payslips.</strong>
                        </div>
                        <p></p></br>
                    </div>
                    {{--@endif--}}
                    @endif
                </ul>
            </div>
        </div>
        @show

        <div class="main-panel">
            @section('navbar')
                <nav class="navbar navbar-expand-lg ">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-minimize">
                            <button id="minimizeSidebar" class="btn btn-success btn-fill btn-round btn-icon d-none d-lg-block">
                                <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                                <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                            </button>
                        </div>
                        <a class="navbar-brand" href="#"> {{ isset($page) ? $page : '' }} </a>
                    </div>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="https://example.com/" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="nc-icon nc-bullet-list-67"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{ url('/loans/create') }}">Loan Application</a>
                                    <a class="dropdown-item" href="{{ url ('loans/personaldisclosureform') }}">Personal Disclosure Form</a>
                                    <a class="dropdown-item" href="{{ url ('loans/pledgedepositform') }}">Pledge of Deposit Form</a>
                                    <a class="dropdown-item" href="{{ url ('loans/authoritydeductform') }}">Authority to Deduct Form</a>
                                    <a class="dropdown-item" href="{{ url ('loans/promissorynoteform') }}">Promissory Note Form</a>
                                    <a class="dropdown-item" href="{{ url ('/changePassword') }}">Change Password</a>
                                    <div class="divider"></div>
                                    <a class="dropdown-item text-danger" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="nc-icon nc-button-power"></i> {{ __('Log out') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            @show

            @yield('content')

            @section('footer')
                <footer class="footer">
                    <div class="container">
                        <nav>
                            <ul class="footer-menu">

                            </ul>
                            <p class="copyright text-center">
                                ©
                                <script>
                                    document.write(new Date().getFullYear())
                                </script>
                                <a href="https://www.tigdesignsolutions.com/" target="_blank">TIG Design and Solutions</a>
                            </p>
                        </nav>
                    </div>
                </footer>
            @show
        </div>
    </div>
</body>

<!--   Core JS Files   -->
<script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/popper/popper.js/dist/umd/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/moment/moment.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/datetimepicker.js') }}" type="text/javascript"></script>

@yield('javascript')

<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('js/theme.js') }}" type="text/javascript"></script>
</html>
