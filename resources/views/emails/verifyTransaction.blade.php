@component('mail::message')
# Hello {{ $mailData['name'] }},

We have received your {{ $mailData['transaction']}} application,
If you wish to proceed with further processing,
Please verify by clicking on the button below:

@component('mail::button', ['url' => $mailData['url']])
Verify {{ $mailData['transaction'] }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
