@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-full-page">

    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute">
        <div class="container d-flex flex-row">
            <div class="navbar-collapse justify-content-end" id="navbar">
                <ul class="navbar-nav">
                    <li class="nav-item  active ">
                        <a href="{{ url('login') }}" class="nav-link">
                            <i class="nc-icon nc-mobile"></i> Login
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="full-page section-image" data-color="azure" data-image="https://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/img/full-screen-image-2.jpg" ;>
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="card card-register card-plain">
                    <div class="card-header ">
                        <div class="row  justify-content-center">
                            <div class="col-md-8 text-center">
                                <div class="header-text">
                                    <h2 class="card-title">We have receieved your registration, Thank you!</h2>
                                    <hr />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-watch-time"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Office Hours</h4>
                                        <table class="table table-borderless text-white table-condensed">
                                            <tbody>
                                                <tr>
                                                    <td>Monday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Tuesday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Wednesday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Thursday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Friday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Saturday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Sunday</td>
                                                    <td>Closed</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-mobile"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Contact Number</h4>
                                        <p>
                                            +63 915 546 7726
                                        </p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-email-83"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Email</h4>
                                        <p>
                                            <a href="">lipiemco@tigcoop.com</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mr-auto">
                                <div class="card card-login">
                                    <div class="card-body">
                                        @if (session('resent'))
                                            <div class="alert alert-success" role="alert">
                                                {{ __('A fresh verification link has been sent to your email address.') }}
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <p>{{__('Before proceeding, please check your email: ') }} <span class="text-info">{{ Auth::user()->email }} </span> {{ (' for a verification link:') }}</p>
                                            <p>{{ __('If you did not receive the email') }},</p>
                                            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                                @csrf
                                                <button type="submit" class="btn btn-link p-0 m-0 align-baseline text-warning">{{ __('Click here to request for a new verification link.') }}</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <nav>
            <ul class="footer-menu">

            </ul>
            <p class="copyright text-center">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>
                <a href="https://www.tigdesignsolutions.com/" target="_blank">TIG Design and Solutions</a>
            </p>
        </nav>
    </div>
</footer>
@endsection
