@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-full-page">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-collapse justify-content-end" id="navbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="{{ url('login') }}" class="nav-link">
                            <i class="nc-icon nc-mobile"></i> Login
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a href="{{ url('register') }}" class="nav-link">
                            <i class="nc-icon nc-badge"></i> Register
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
    <div class="full-page register-page section-image" data-color="azure" data-image="https://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/img/bg5.jpg">
        <div class="content">
            <div class="container">
                <div class="card card-register card-plain">
                    <div class="card-header ">
                        <div class="row  justify-content-center">
                            <div class="col-md-8 text-center">
                                <div class="header-text">
                                    <h2 class="card-title">Create Account</h2>
                                    <hr />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-watch-time"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Office Hours</h4>
                                        <table class="table table-borderless text-white table-condensed">
                                            <tbody>
                                            <tr>
                                                <td>Monday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Tuesday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Wednesday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Thursday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Friday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Saturday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Sunday</td>
                                                <td>Closed</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-mobile"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Contact Number</h4>
                                        <p>
                                            +63 915 546 7726
                                        </p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-email-83"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Email</h4>
                                        <p>
                                            <a href="">lipiemco@tigcoop.com</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mr-auto">
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf

                                    <div class="card card-plain">
                                        <div class="content">
                                            <div class="form-group">
                                                <input id="name" type="text" placeholder="Full Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>This email has already been registered. Please <a class="text-dark" href="{{ url('login') }}"><strong>login</strong></a> or use a different email.</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                            </div>

                                            <div class="form-group">
                                                <div class="g-recaptcha" style="transform: scale(1.1); -webkit-transform: scale(1.1); transform-origin: 0 0; -webkit-transform-origin: 0 0;" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>
                                                @error('g-recaptcha-response')
                                                <span class="invalid-feedback" style="display:block">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="footer text-center">
                                            <button type="submit" class="btn btn-fill btn-neutral btn-wd">Create Account</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
