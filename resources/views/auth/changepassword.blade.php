@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-full-page">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-collapse justify-content-end" id="navbar">
                <ul class="navbar-nav">
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
    <div class="full-page register-page section-image" data-color="orange" data-image="https://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/img/bg5.jpg">
        <div class="content">
            <div class="container">
                <div class="card card-register card-plain">
                    <div class="card-header ">
                        <div class="row  justify-content-center">
                            <div class="col-md-8 text-center">
                                <div class="header-text">
                                    <h2 class="card-title">Change Password</h2>
                                    <hr />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-badge"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Dr. Dentist Cares</h4>
                                        <p>
                                            General Dentistry
                                        </p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-watch-time"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Clinic Schedules</h4>
                                        <table class="table table-borderless text-white table-condensed">
                                            <tbody>
                                            <tr>
                                                <td>Monday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Tuesday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Wednesday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Thursday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Friday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Saturday</td>
                                                <td>8AM - 6PM</td>
                                            </tr>
                                            <tr>
                                                <td>Sunday</td>
                                                <td>Closed</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-mobile"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Contact Numbers</h4>
                                        <p>
                                            +63 915 546 7726
                                        </p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-email-83"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Email</h4>
                                        <p>
                                            <a href="mailto:dentalcares@tigdental.com">dentistcares@tigdental.com</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mr-auto">
                                <form method="POST" action="{{ route('changePassword') }}">
                                    {{ csrf_field() }}

                                    <div class="card card-plain">
                                        <div class="content">
                                            @if (session('error'))
                                                <div class="alert alert-danger">
                                                    {{ session('error') }}
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success">
                                                    {{ session('success') }}
                                                </div>
                                            @endif
                                            <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                                <input id="current-password" placeholder="Current Password" type="password" class="form-control" name="current-password" required>

                                                @if ($errors->has('current-password'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('current-password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                                <input id="new-password" placeholder="New Password" type="password" class="form-control" name="new-password" required>

                                                @if ($errors->has('new-password'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('new-password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <input id="new-password-confirm" placeholder="Confirm New Password" type="password" class="form-control" name="new-password_confirmation" required>
                                            </div>
                                        </div>
                                        <div class="footer text-center">
                                            <button type="submit" class="btn btn-primary">Change Password</button>
                                            <a href="javascript:history.back()" class="btn btn-fill btn-secondary">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer"></footer>
</div>
@endsection
