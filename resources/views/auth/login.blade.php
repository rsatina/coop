@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-full-page">

    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute">
        <div class="container d-flex flex-row">
            <div class="navbar-collapse justify-content-end" id="navbar">
                <ul class="navbar-nav">
                    <li class="nav-item  active ">
                        <a href="{{ url('login') }}" class="nav-link">
                            <i class="nc-icon nc-mobile"></i> Login
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="{{ url('register') }}" class="nav-link">
                            <i class="nc-icon nc-badge"></i> Register
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="full-page section-image" data-color="azure" data-image="https://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/img/full-screen-image-2.jpg" ;>
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="card card-register card-plain">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-watch-time"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Office Hours</h4>
                                        <table class="table table-borderless text-white table-condensed">
                                            <tbody>
                                                <tr>
                                                    <td>Monday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Tuesday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Wednesday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Thursday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Friday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Saturday</td>
                                                    <td>8AM - 6PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Sunday</td>
                                                    <td>Closed</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-mobile"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Contact Number</h4>
                                        <p>
                                            +63 915 546 7726
                                        </p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-email-83"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Email</h4>
                                        <p>
                                            <a href="">lipiemco@tigcoop.com</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mr-auto">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="card card-login">
                                        <div class="card-header ">
                                            <h3 class="header text-center">
                                                <img src="{{asset('images/lipiemco-logo.png')}}" width="120" height="120"/>
                                            </h3>
                                        </div>
                                        @if (session('status'))
                                            <div class="alert alert-success">
                                                {{ session('status') }}
                                            </div>
                                        @endif
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="email">{{ __('Email') }}</label>

                                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror  @error('email_reg') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                @error('email_reg')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }} <a href="{{ url('register') }}">register</a> {{ __('to access the system.') }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="password">{{ __('Password') }}</label>

                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
				                                <p style="font-size:13px;text-align:right"><a href="{{ url('password/reset') }}">Forgot Password?</a></p>
                                            </div>
                                            <div class="form-group">
                                                <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>
                                                @error('g-recaptcha-response')
                                                <span class="invalid-feedback" style="display:block">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            {{--<div class="form-group">--}}
                                            {{--<div class="form-check">--}}
                                            {{--<label class="form-check-label" for="remember">--}}
                                            {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}
                                            {{--<span class="form-check-sign"></span>--}}
                                            {{--{{ __('Remember Me') }}--}}
                                            {{--</label>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                        </div>
                                        <div class="card-footer ml-auto mr-auto">
                                            <button type="submit" class="btn btn-warning btn-wd">{{ __('Login') }}</button>
                                    </div>
                                </form>
                                <p class="text-center">
                                    <a  data-toggle="modal" data-target="#exampleModalScrollable" style="cursor:pointer;"><strong><u>Privacy Policy</u></strong></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #FFA500; padding:15px 15px;">
                    <h6 class="modal-title" id="exampleModalScrollableTitle">Privacy Policy</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-justify">
                    <p>We value your privacy as much as we value your health. 
                        The clinic is committed to comply with Republic Act No. 
                        10173 also known as the Philippine Data Privacy Act of 2012 (“Data Privacy Act”).</p>
                    <p>We will update our Notice from time to time when necessary 
                        to ensure that we will remain compliant with the applicable laws.</p>
                    <p>Why do we collect your personal information?</p>
                    <p>We collect your personal information to:</p>
                    <p>
                        <ul>
                            <li>Process, review and effectively manage your appointments;</li>
                            <li>Keep an accurate record of our patients and the services we have rendered;</li>
                            <li>Conduct studies and researches to continually develop and improve our service offerings and clinic practices;</li>
                            <li>Perform profile analysis and analytics to further understand situational needs and preferences of patients to effectively improve and prescribe services;</li>
                        </ul>
                    </p>
                    <p>What type of personal information do we collect from you?</p>
                    <p>The information we collect and the process of collection include:</p>
                    <p>
                        <ul>
                            <li>Personal and demographic information:  complete name, complete address, email address, contact numbers, gender, nationality, civil status, age, birthdate and any other information that we deem necessary in managing your appointment and clinic record;</li>
                            <li>Employment information:  company name, company address, company contact number, position</li>
                            <li>Medical insurance provider</li>
                            <li>Medical history such as your current medical condition, previous diagnosis for ailments, applicable medical orders and records, etc.;</li>
                        </ul>
                    </p>
                    <p>Why do we use your personal information?</p>
                    <p>We use your personal information in order for us to perform transactions and functions necessary 
                    to implement and administer the healthcare benefit you purchased or availed from Maxicare. 
                    Your personal information may also be used for reporting, analysis and study purposes and other functions
                     required or permitted by law.</p>
                    <p>We share your personal information to ensure that we can provide you with the best and 
                    excellent healthcare services that is readily available and that you can experience a 
                    smoother and better business relationship with us. For example, we may outsource the 
                    maintenance of our database, website or we may share your information to our affiliated hospitals, 
                    clinics and physicians for them to provide you with immediate medical services. At any rate, 
                    we only share your information to our Representatives that is necessary for them to perform their 
                    functions and to the extent permitted for by law.</p>
                    <p>Where do we get your personal information?</p>
                    <p>
                        <ul>
                            <li>Filled-out application forms, agreements and other related documents;</li>
                            <li>When you register and/or login to our website, mobile application and other web-based systems;</li>
                            <li>When you contact our clinic to make inquiry, book an appointment or request for service;</li>
                            <li>When accessing our web-based and electronic platforms; Third Party web analytics tools, 
                            including those of third parties’, that use cookies to collect anonymous information and data 
                            generated in connection with your activities when you visit the pages.</li>
                        </ul>
                    </p>
                    <p>When and to whom do we disclose or share your personal information?</p>
                    <p>We will never share or disclose your personal information and medical records to 
                    third parties outside of our clinic’s authorized representatives unless you have given your consent.</p>
                    <p>How do we protect your personal information?</p>
                    <p>Your privacy is of utmost concern to us that is why we maintain, enforce and implement physical, 
                    technical, electronic and procedural security measures that ensure the confidentiality and security 
                    of your information.</p>
                    <p>We protect your information by implementing safeguards such as but not limited to the following:</p>
                    <p>
                        <ul>
                            <li>Limited and restricted access to your personal information only as necessary in as 
                            may be required by the doctor and/or clinic personnel;</li>
                            <li>We use a secured server that is maintained with appropriate security and data integrity controls;</li>
                            <li>We have signed confidentiality and non-disclosure agreements with contracted parties 
                            that may directly or indirectly have access to our systems;</li>
                        </ul>
                    </p>
                    <p>Your information is kept in a secured cloud service providers and will reside their for 
                    long as necessary for purposes of maintaining your clinical record and for reference, as necessary, 
                    for whatever legal requirement.</p>
                    <p>Your rights to your personal information</p>
                    <p>In accordance with the Data Privacy Act, we respect and uphold your right in relation 
                    to your personal information. It is your right to be informed on the processing of your 
                    personal information. As such, you have the right to object to our collection or processing 
                    of your personal information. You also have the right to access, to correct or modify, or request 
                    to discontinue the use of your personal information from the systems and processes of the clinic.  
                    However, choosing not to disclose or removal of your personal information from our systems may limit 
                    and/or hinder our ability to provide our services to you.</p>
                    <p>Contact Information</p>
                    <p>For questions and concerns regarding the use of your personal information, you may contact us at:</p>
                    <p>Contact Number: +63 915 546 7726<br> Email: dentistcares@tigdental.com</p>
                </div>
                <div class="modal-footer" style="background-color: #f9f9f9; padding:15px 15px;">
                    <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

</div>
<footer class="footer">
    <div class="container">
        <nav>
            <ul class="footer-menu">

            </ul>
            <p class="copyright text-center">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>
                <a href="https://www.tigdesignsolutions.com/" target="_blank">TIG Design and Solutions</a>
            </p>
        </nav>
    </div>
</footer>

@endsection
